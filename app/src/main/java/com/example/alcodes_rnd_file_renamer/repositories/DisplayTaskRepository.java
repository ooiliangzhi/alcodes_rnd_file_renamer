package com.example.alcodes_rnd_file_renamer.repositories;

import android.content.Context;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.example.alcodes_rnd_file_renamer.adapters.DisplayTaskAdapter;
import com.example.alcodes_rnd_file_renamer.adapters.EditTaskAdapter;
import com.example.alcodes_rnd_file_renamer.database.entities.InsertCharacter;
import com.example.alcodes_rnd_file_renamer.database.entities.InsertCharacterDao;
import com.example.alcodes_rnd_file_renamer.database.entities.InsertFileSize;
import com.example.alcodes_rnd_file_renamer.database.entities.InsertFileSizeDao;
import com.example.alcodes_rnd_file_renamer.database.entities.InsertNumber;
import com.example.alcodes_rnd_file_renamer.database.entities.InsertNumberDao;
import com.example.alcodes_rnd_file_renamer.database.entities.MyTask;
import com.example.alcodes_rnd_file_renamer.database.entities.MyTaskDao;
import com.example.alcodes_rnd_file_renamer.database.entities.NewName;
import com.example.alcodes_rnd_file_renamer.database.entities.NewNameDao;
import com.example.alcodes_rnd_file_renamer.database.entities.ReplaceCharacters;
import com.example.alcodes_rnd_file_renamer.database.entities.ReplaceCharactersDao;
import com.example.alcodes_rnd_file_renamer.utils.DatabaseHelper;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class DisplayTaskRepository {

    private static DisplayTaskRepository mInstance;

    private MutableLiveData<MyTask> task = new MutableLiveData<>();

    private MutableLiveData<List<DisplayTaskAdapter.DataHolder>> taskList = new MutableLiveData<>();

    private MutableLiveData<List<EditTaskAdapter.DataHolder>> taskCriterion = new MutableLiveData<>();

    public static DisplayTaskRepository getInstance() {
        if (mInstance == null) {
            synchronized (DisplayTaskRepository.class) {
                mInstance = new DisplayTaskRepository();
            }
        }
        return  mInstance;
    }

    private DisplayTaskRepository(){
    }

    public LiveData<List<DisplayTaskAdapter.DataHolder>> getDisplayTaskAdapterListLiveData() {
        return taskList;
    }

    public void loadDisplayTaskAdapterList(Context context) {
        List<DisplayTaskAdapter.DataHolder> dataHolders = new ArrayList<>();
        List<MyTask> records = DatabaseHelper.getInstance(context)
                .getMyTaskDao()
                .loadAll();

        if (records != null) {
            for (MyTask myTask : records) {
                DisplayTaskAdapter.DataHolder dataHolder = new DisplayTaskAdapter.DataHolder();
                dataHolder.taskId = myTask.getTaskId();
                dataHolder.taskName = myTask.getTaskName();
                dataHolder.taskCreateDate = myTask.getTaskCreateDate();

                dataHolders.add(dataHolder);
            }
        }

        taskList.setValue(dataHolders);
    }

    public void deleteTask(Context context, Long id) {

        DatabaseHelper.getInstance(context)
                .queryBuilder(NewName.class)
                .where(NewNameDao.Properties.TaskId.eq(id))
                .buildDelete()
                .executeDeleteWithoutDetachingEntities();

        DatabaseHelper.getInstance(context)
                .queryBuilder(ReplaceCharacters.class)
                .where(ReplaceCharactersDao.Properties.TaskId.eq(id))
                .buildDelete()
                .executeDeleteWithoutDetachingEntities();

        DatabaseHelper.getInstance(context)
                .queryBuilder(InsertCharacter.class)
                .where(InsertCharacterDao.Properties.TaskId.eq(id))
                .buildDelete()
                .executeDeleteWithoutDetachingEntities();

        DatabaseHelper.getInstance(context)
                .queryBuilder(InsertNumber.class)
                .where(InsertNumberDao.Properties.TaskId.eq(id))
                .buildDelete()
                .executeDeleteWithoutDetachingEntities();

        DatabaseHelper.getInstance(context)
                .queryBuilder(InsertFileSize.class)
                .where(InsertFileSizeDao.Properties.TaskId.eq(id))
                .buildDelete()
                .executeDeleteWithoutDetachingEntities();

        // Delete record from database.
        DatabaseHelper.getInstance(context)
                .getMyTaskDao()
                .deleteByKey(id);

        // Done deleting record, now re-load list.
        loadDisplayTaskAdapterList(context);
    }

    public void loadSingleTask(Context context, Long id){
        int numberCount = 1;
        MyTask myTask = DatabaseHelper.getInstance(context)
                .getMyTaskDao()
                .load(id);

        task.setValue(myTask);

        List<EditTaskAdapter.DataHolder> dataHolders = new ArrayList<>();

        if (myTask.getReplaceCharacters().size() != 0){
            String matchCaseString;
            for(int i = 0; i < myTask.getReplaceCharacters().size(); i++){
                EditTaskAdapter.DataHolder dataHolder = new EditTaskAdapter.DataHolder();

                if(myTask.getReplaceCharacters().get(i).getMatchCase()){
                    matchCaseString = "case sensitive";
                }else {
                    matchCaseString = "case insensitive";
                }

                dataHolder.criterion_id = myTask.getReplaceCharacters().get(i).getReplaceCharacterId();
                dataHolder.number = numberCount;
                dataHolder.title = "Replace Characters";
                dataHolder.detail = "Replace \"" + myTask.getReplaceCharacters().get(i).getCharacterToReplace()+ "\" with \"" +
                        myTask.getReplaceCharacters().get(i).getReplaceWith() + "\", " + matchCaseString;;
                numberCount++;
                dataHolders.add(dataHolder);
            }
        }

        if (myTask.getInsertCharacters().size() != 0){
            String charInsertAtString = "";
            for(int i = 0; i < myTask.getInsertCharacters().size(); i++){
                EditTaskAdapter.DataHolder dataHolder = new EditTaskAdapter.DataHolder();

                if(myTask.getInsertCharacters().get(i).getInsertAt() == 0){
                    charInsertAtString = "begin of name";
                }else if(myTask.getInsertCharacters().get(i).getInsertAt() == -1){
                    charInsertAtString = "end of name";
                }else {
                    charInsertAtString = String.valueOf(myTask.getInsertCharacters().get(i).getInsertAt());
                }

                dataHolder.criterion_id = myTask.getInsertCharacters().get(i).getInsertCharacterId();
                dataHolder.number = numberCount;
                dataHolder.title = "Insert Characters";
                dataHolder.detail = "Insert \"" + myTask.getInsertCharacters().get(i).getCharacterToInsert() + "\" at position \"" + charInsertAtString + "\"";

                numberCount++;
                dataHolders.add(dataHolder);
            }
        }

        if (myTask.getNewName().size() != 0){
            for(int i = 0; i < myTask.getNewName().size(); i++){
                EditTaskAdapter.DataHolder dataHolder = new EditTaskAdapter.DataHolder();
                dataHolder.criterion_id = myTask.getNewName().get(i).getNewNameId();
                dataHolder.number = numberCount;
                dataHolder.title = "New Name";
                dataHolder.detail = "Set name to \"" + myTask.getNewName().get(i).getNewName() +"\"";

                numberCount++;
                dataHolders.add(dataHolder);
            }
        }

        if (myTask.getInsertNumbers().size() != 0){
            String numInsertAtString = "";
            String autoMethodString = "";
            for(int i = 0; i < myTask.getInsertNumbers().size(); i++){
                EditTaskAdapter.DataHolder dataHolder = new EditTaskAdapter.DataHolder();

                if (myTask.getInsertNumbers().get(i).getAutoNumber() == 1){
                    autoMethodString =" with increment";
                }else if (myTask.getInsertNumbers().get(i).getAutoNumber() == -1){
                    autoMethodString =" with decrement";
                }else if (myTask.getInsertNumbers().get(i).getAutoNumber() == 0){
                    autoMethodString ="";
                }


                if (myTask.getInsertNumbers().get(i).getInsertAt() == 0){
                    numInsertAtString = "begin of name";
                }else if (myTask.getInsertNumbers().get(i).getInsertAt() == -1){
                    numInsertAtString = "end of name";
                }else {
                    numInsertAtString = String.valueOf(myTask.getInsertNumbers().get(i).getInsertAt());
                }

                dataHolder.criterion_id = myTask.getInsertNumbers().get(i).getInsertNumberId();
                dataHolder.number = numberCount;
                dataHolder.title = "Insert Number";
                dataHolder.detail = "Insert \"" + myTask.getInsertNumbers().get(i).getNumberToInsert() + "\" at position \"" + numInsertAtString + "\""+ autoMethodString;;

                numberCount++;
                dataHolders.add(dataHolder);
            }
        }

        if (myTask.getInsertFileSizes().size() != 0){
            String fileSizeInsertAtString;
            for(int i = 0; i < myTask.getInsertFileSizes().size(); i++){
                EditTaskAdapter.DataHolder dataHolder = new EditTaskAdapter.DataHolder();

                if (myTask.getInsertFileSizes().get(i).getInsertAt() == 0){
                    fileSizeInsertAtString = "begin of name";
                }else if (myTask.getInsertFileSizes().get(i).getInsertAt() == -1){
                    fileSizeInsertAtString = "end of name";
                }else {
                    fileSizeInsertAtString = String.valueOf(myTask.getInsertFileSizes().get(i).getInsertAt());
                }

                dataHolder.criterion_id = myTask.getInsertFileSizes().get(i).getInsertFileSizeId();
                dataHolder.number = numberCount;
                dataHolder.title = "Insert File Size";
                dataHolder.detail = "Insert size with format \"" + myTask.getInsertFileSizes().get(i).getFormatSize() + "\", \"" + myTask.getInsertFileSizes().get(i).getDecimalPlace() + "\" d.p at position \""+ fileSizeInsertAtString + "\"";

                numberCount++;
                dataHolders.add(dataHolder);
            }
        }
        taskCriterion.setValue(dataHolders);

    }

    public LiveData<List<EditTaskAdapter.DataHolder>> getLoadSingleTaskCriterion(){
        return taskCriterion;
    }

    public LiveData<MyTask> getLoadSingleTask(){
        return task;
    }

    public void addReplaceCharacters(Context context, ReplaceCharacters replaceCharacters, Long taskId){
        replaceCharacters.setTaskId(taskId);
        DatabaseHelper.getInstance(context)
                .getReplaceCharactersDao()
                .insert(replaceCharacters);

        DatabaseHelper.getInstance(context)
                .getMyTaskDao()
                .load(taskId)
                .resetReplaceCharacters();

        loadSingleTask(context, taskId);
    }

    public void addNewName(Context context, NewName newName, Long taskId) {
        newName.setTaskId(taskId);
        DatabaseHelper.getInstance(context)
                .getNewNameDao()
                .insert(newName);

        DatabaseHelper.getInstance(context)
                .getMyTaskDao()
                .load(taskId)
                .resetNewName();

        loadSingleTask(context, taskId);

    }

    public void addInsertCharacter(Context context, InsertCharacter insertCharacter, Long taskId) {
        insertCharacter.setTaskId(taskId);
        DatabaseHelper.getInstance(context)
                .getInsertCharacterDao()
                .insert(insertCharacter);

        DatabaseHelper.getInstance(context)
                .getMyTaskDao()
                .load(taskId)
                .resetInsertCharacters();

        loadSingleTask(context, taskId);
    }

    public void addInsertNumber(Context context, InsertNumber insertNumber, Long taskId) {
        insertNumber.setTaskId(taskId);
        DatabaseHelper.getInstance(context)
                .getInsertNumberDao()
                .insert(insertNumber);

        DatabaseHelper.getInstance(context)
                .getMyTaskDao()
                .load(taskId)
                .resetInsertNumbers();

        loadSingleTask(context, taskId);
    }

    public void addInsertFileSize(Context context, InsertFileSize insertFileSize, Long taskId) {
        insertFileSize.setTaskId(taskId);
        DatabaseHelper.getInstance(context)
                .getInsertFileSizeDao()
                .insert(insertFileSize);

        DatabaseHelper.getInstance(context)
                .getMyTaskDao()
                .load(taskId)
                .resetInsertFileSizes();

        loadSingleTask(context, taskId);
    }

    public void deleteCriterion(Context context, EditTaskAdapter.DataHolder data, Long taskId) {
        String criterionType = data.title;
        Long CriterionId = data.criterion_id;

        if (criterionType == "Replace Characters"){
            DatabaseHelper.getInstance(context)
                    .getReplaceCharactersDao()
                    .deleteByKey(CriterionId);

            DatabaseHelper.getInstance(context)
                    .getMyTaskDao()
                    .load(taskId)
                    .resetReplaceCharacters();
        }else if (criterionType == "Insert Characters"){
            DatabaseHelper.getInstance(context)
                    .getInsertCharacterDao()
                    .deleteByKey(CriterionId);

            DatabaseHelper.getInstance(context)
                    .getMyTaskDao()
                    .load(taskId)
                    .resetInsertCharacters();
        }else if (criterionType == "New Name"){
            DatabaseHelper.getInstance(context)
                    .getNewNameDao()
                    .deleteByKey(CriterionId);

            DatabaseHelper.getInstance(context)
                    .getMyTaskDao()
                    .load(taskId)
                    .resetNewName();
        }else if (criterionType == "Insert Number"){
            DatabaseHelper.getInstance(context)
                    .getInsertNumberDao()
                    .deleteByKey(CriterionId);

            DatabaseHelper.getInstance(context)
                    .getMyTaskDao()
                    .load(taskId)
                    .resetInsertNumbers();
        }else if (criterionType == "Insert File Size") {
            DatabaseHelper.getInstance(context)
                    .getInsertFileSizeDao()
                    .deleteByKey(CriterionId);

            DatabaseHelper.getInstance(context)
                    .getMyTaskDao()
                    .load(taskId)
                    .resetInsertFileSizes();
        }
        loadSingleTask(context, taskId);
    }

    public void updateTaskName(Context context, String taskName, Long taskId) {
        MyTaskDao myTaskDao = DatabaseHelper.getInstance(context).getMyTaskDao();
        MyTask myTask = myTaskDao.load(taskId);

        Date date = Calendar.getInstance().getTime();

        myTask.setTaskName(taskName);
        myTask.setTaskCreateDate(date);

        loadDisplayTaskAdapterList(context);
    }
}
