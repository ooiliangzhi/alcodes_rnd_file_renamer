package com.example.alcodes_rnd_file_renamer.repositories;

import android.app.Application;
import android.content.Context;
import android.provider.DocumentsContract;
import android.util.Log;
import android.widget.Toast;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.example.alcodes_rnd_file_renamer.adapters.DisplaySelectionTaskAdapter;
import com.example.alcodes_rnd_file_renamer.adapters.PreviewFolderAdapter;
import com.example.alcodes_rnd_file_renamer.adapters.SelectFolderAdapter;
import com.example.alcodes_rnd_file_renamer.database.entities.MyTask;
import com.example.alcodes_rnd_file_renamer.utils.DatabaseHelper;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

public class SelectFolderRepository {

    private static SelectFolderRepository mInstance;

    private MutableLiveData<List<PreviewFolderAdapter.DataHolder>> previewFileNameList = new MutableLiveData<>();

    private MutableLiveData<List<DisplaySelectionTaskAdapter.DataHolder>> taskList = new MutableLiveData<>();

    private MutableLiveData<List<SelectFolderAdapter.DataHolder>> mSelectFolderAdapterListLiveData = new MutableLiveData<>();

    public static SelectFolderRepository getInstance() {
        if (mInstance == null) {
            synchronized (SelectFolderRepository.class) {
                mInstance = new SelectFolderRepository();
            }
        }
        return mInstance;
    }

    private SelectFolderRepository(){
    }

    public LiveData<List<SelectFolderAdapter.DataHolder>> getSelectFolderAdapterListLiveData() {
        return mSelectFolderAdapterListLiveData;
    }

    public void loadSelectFolderAdapterList(Context context, List<SelectFolderAdapter.DataHolder> fileDetailModelList) {
        List<SelectFolderAdapter.DataHolder> dataHolders = new ArrayList<>();

        for(int i=0; i<fileDetailModelList.size(); i++){
            SelectFolderAdapter.DataHolder dataHolder = new SelectFolderAdapter.DataHolder();
            dataHolder.uri = fileDetailModelList.get(i).uri;
            dataHolder.fileName = fileDetailModelList.get(i).fileName;
            dataHolder.date = fileDetailModelList.get(i).date;
            dataHolder.fileSize = fileDetailModelList.get(i).fileSize;
            dataHolder.fileType = fileDetailModelList.get(i).fileType;
            dataHolder.check = fileDetailModelList.get(i).check;

            dataHolders.add(dataHolder);
        }
        mSelectFolderAdapterListLiveData.setValue(dataHolders);
    }

    public void updateSingleSelectFolder(Context context, SelectFolderAdapter.DataHolder data) {
        List<SelectFolderAdapter.DataHolder> dataHolders = mSelectFolderAdapterListLiveData.getValue();
        int getIndex = dataHolders.indexOf(data);

        if(data.check){
            data.check = false;
        }else{
            data.check = true;
        }
        dataHolders.set(getIndex, data);
        loadSelectFolderAdapterList(context, dataHolders);
    }

    public void updateSelectFolderAdapterList(Context context, boolean checkSelectedAll) {
        List<SelectFolderAdapter.DataHolder> dataHolders = mSelectFolderAdapterListLiveData.getValue();

        for(int i =0; i < dataHolders.size(); i++) {
            dataHolders.get(i).check=checkSelectedAll;
        }
        loadSelectFolderAdapterList(context, dataHolders);
    }

    public void loadDisplaySelectionTaskAdapterList(Context context){
        List<DisplaySelectionTaskAdapter.DataHolder> dataHolders = new ArrayList<>();
        List<MyTask> records = DatabaseHelper.getInstance(context)
                .getMyTaskDao()
                .loadAll();

        if (records != null) {
            for (MyTask myTask : records) {
                DisplaySelectionTaskAdapter.DataHolder dataHolder = new DisplaySelectionTaskAdapter.DataHolder();
                dataHolder.taskId = myTask.getTaskId();
                dataHolder.taskName = myTask.getTaskName();
                dataHolder.taskCreateDate = myTask.getTaskCreateDate();

                dataHolders.add(dataHolder);
            }
        }

        taskList.setValue(dataHolders);
    }

    public LiveData<List<DisplaySelectionTaskAdapter.DataHolder>> getLoadDisplaySelectionTaskAdapterList() {
        return taskList;
    }

    public void updateDisplaySelectionTaskAdapter(Context context, DisplaySelectionTaskAdapter.DataHolder data) {

        int getIndex = taskList.getValue().indexOf(data);

        for(int i =0; i < taskList.getValue().size(); i++) {
            taskList.getValue().get(i).check=false;
        }

        if(data.check){
            data.check = false;
        }else{
            data.check = true;
        }
        taskList.getValue().set(getIndex, data);

    }

    public void loadPreviewRenameFile(Context context) {
        List<PreviewFolderAdapter.DataHolder> dataHolders = new ArrayList<>();
        boolean autoFlag = false;
        int countAutoNumber =0;

        Long taskId = 0L;
        MyTask myTask = new MyTask();

        for(int i = 0; i < taskList.getValue().size(); i++){
            if (taskList.getValue().get(i).check==true) {
                taskId = taskList.getValue().get(i).taskId;
                myTask = DatabaseHelper.getInstance(context)
                        .getMyTaskDao()
                        .load(taskId);
                break;
            }
        }



        if(taskId != 0L) {
            for (int i = 0; i < mSelectFolderAdapterListLiveData.getValue().size(); i++) {
                if (mSelectFolderAdapterListLiveData.getValue().get(i).check) {

                    String newFileName = mSelectFolderAdapterListLiveData.getValue().get(i).fileName;
                    Long fileSize = mSelectFolderAdapterListLiveData.getValue().get(i).fileSize;

                    PreviewFolderAdapter.DataHolder dataHolder = new PreviewFolderAdapter.DataHolder();

                    if (myTask.getNewName().size() != 0) {
                        for (int x = 0; x < myTask.getNewName().size(); x++) {
                            newFileName = myTask.getNewName().get(x).getNewName();
                        }
                    }

                    if (myTask.getReplaceCharacters().size() != 0) {
                        for (int x = 0; x < myTask.getReplaceCharacters().size(); x++) {
                            if (myTask.getReplaceCharacters().get(x).getMatchCase()) {
                                newFileName = newFileName.replaceAll(myTask.getReplaceCharacters().get(x).getCharacterToReplace(), myTask.getReplaceCharacters().get(x).getReplaceWith());
                            } else {
                                newFileName = newFileName.replaceAll("(?i)" + myTask.getReplaceCharacters().get(x).getCharacterToReplace(), myTask.getReplaceCharacters().get(x).getReplaceWith());
                            }
                        }
                    }

                    if (myTask.getInsertCharacters().size() != 0) {
                        for (int x = 0; x < myTask.getInsertCharacters().size(); x++) {
                            if (myTask.getInsertCharacters().get(x).getInsertAt() == 0) {
                                newFileName = myTask.getInsertCharacters().get(x).getCharacterToInsert() + newFileName;
                            } else if (myTask.getInsertCharacters().get(x).getInsertAt() == -1) {
                                newFileName = newFileName + myTask.getInsertCharacters().get(x).getCharacterToInsert();
                            } else {
                                String temp = new String();
                                for (int z = 0; z < newFileName.length(); z++) {

                                    // Insert the original string character
                                    // into the new string
                                    temp += newFileName.charAt(z);

                                    if (z == myTask.getInsertCharacters().get(x).getInsertAt()) {

                                        // Insert the string to be inserted
                                        // into the new string
                                        temp += myTask.getInsertCharacters().get(x).getCharacterToInsert();
                                    }
                                }
                                newFileName = temp;
                            }
                        }
                    }

                    if (myTask.getInsertNumbers().size() != 0) {
                        for (int x = 0; x < myTask.getInsertNumbers().size(); x++) {
                            if (autoFlag == false) {
                                autoFlag = true;
                                countAutoNumber = Integer.parseInt(myTask.getInsertNumbers().get(x).getNumberToInsert());
                            }

                            if (myTask.getInsertNumbers().get(x).getAutoNumber() == 1) {
                                countAutoNumber++;
                            } else if (myTask.getInsertNumbers().get(x).getAutoNumber() == -1) {
                                countAutoNumber--;
                            } else if (myTask.getInsertNumbers().get(x).getAutoNumber() == 0) {
                                countAutoNumber = countAutoNumber + 0;
                            }


                            if (myTask.getInsertNumbers().get(x).getInsertAt() == 0) {
                                newFileName = countAutoNumber + newFileName;
                            } else if (myTask.getInsertNumbers().get(x).getInsertAt() == -1) {
                                newFileName = newFileName + countAutoNumber;
                            } else {
                                String temp = new String();
                                for (int z = 0; z < newFileName.length(); z++) {

                                    // Insert the original string character
                                    // into the new string
                                    temp += newFileName.charAt(z);

                                    if (z == myTask.getInsertNumbers().get(x).getInsertAt()) {

                                        // Insert the string to be inserted
                                        // into the new string
                                        temp += countAutoNumber;
                                    }
                                }
                                newFileName = temp;
                            }
                        }
                    }

                    if (myTask.getInsertFileSizes().size() != 0) {
                        String s = "";
                        long n = 1024;
                        double kb = fileSize / n;
                        double mb = kb / n;
                        double gb = mb / n;
                        double tb = gb / n;
                        for (int x = 0; x < myTask.getInsertFileSizes().size(); x++) {
                            if (myTask.getInsertFileSizes().get(x).getFormatSize() == "Auto") {
                                if (fileSize < n) {
                                    s = fileSize + " Bytes";
                                } else if (fileSize >= n && fileSize < (n * n)) {
                                    s = String.format("%.2f", kb) + " KB";
                                } else if (fileSize >= (n * n) && fileSize < (n * n * n)) {
                                    s = String.format("%.2f", mb) + " MB";
                                } else if (fileSize >= (n * n * n) && fileSize < (n * n * n * n)) {
                                    s = String.format("%.2f", gb) + " GB";
                                } else if (fileSize >= (n * n * n * n)) {
                                    s = String.format("%.2f", tb) + " TB";
                                }
                            } else if (myTask.getInsertFileSizes().get(x).getFormatSize() == "Byte") {
                                s = fileSize + " Bytes";
                            } else if (myTask.getInsertFileSizes().get(x).getFormatSize() == "KB") {
                                s = String.format("%.2f", kb) + " KB";
                            } else if (myTask.getInsertFileSizes().get(x).getFormatSize() == "MB") {
                                s = String.format("%.2f", mb) + " MB";
                            } else if (myTask.getInsertFileSizes().get(x).getFormatSize() == "GB") {
                                s = String.format("%.2f", gb) + " GB";
                            } else if (myTask.getInsertFileSizes().get(x).getFormatSize() == "TB") {
                                s = String.format("%.2f", tb) + " TB";
                            }

                            if (myTask.getInsertFileSizes().get(x).getInsertAt() == 0) {
                                newFileName = s + newFileName;
                            } else if (myTask.getInsertFileSizes().get(x).getInsertAt() == -1) {
                                newFileName = newFileName + s;
                            } else {
                                String temp = new String();
                                for (int z = 0; z < newFileName.length(); z++) {

                                    // Insert the original string character
                                    // into the new string
                                    temp += newFileName.charAt(z);

                                    if (z == myTask.getInsertFileSizes().get(x).getInsertAt()) {

                                        // Insert the string to be inserted
                                        // into the new string
                                        temp += s;
                                    }
                                }
                                newFileName = temp;
                            }

                        }
                    }

                    dataHolder.uri = mSelectFolderAdapterListLiveData.getValue().get(i).uri;
                    dataHolder.OriginalName = mSelectFolderAdapterListLiveData.getValue().get(i).fileName;
                    dataHolder.NewName = newFileName;

                    dataHolders.add(dataHolder);
                }

            }
            previewFileNameList.setValue(dataHolders);
        }
        else{
            previewFileNameList.setValue(null);
        }


    }

    public LiveData<List<PreviewFolderAdapter.DataHolder>> getLoadPreviewRenameFile(){
        return previewFileNameList;
    }

    public void saveRenameFile(Context context) {
        for(int i = 0; i < previewFileNameList.getValue().size(); i++){
            try {
                DocumentsContract.renameDocument(context.getContentResolver(), previewFileNameList.getValue().get(i).uri
                        , previewFileNameList.getValue().get(i).NewName);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
    }
}
