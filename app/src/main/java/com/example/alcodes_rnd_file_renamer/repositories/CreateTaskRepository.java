package com.example.alcodes_rnd_file_renamer.repositories;

import android.content.Context;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.example.alcodes_rnd_file_renamer.adapters.CreateTaskAdapter;
import com.example.alcodes_rnd_file_renamer.database.entities.MyTask;
import com.example.alcodes_rnd_file_renamer.utils.DatabaseHelper;

import java.util.ArrayList;
import java.util.List;

public class CreateTaskRepository {

    private static CreateTaskRepository mInstance;

    private MutableLiveData<List<CreateTaskAdapter.DataHolder>> taskDetail = new MutableLiveData<>();

    public static CreateTaskRepository getInstance() {
        if (mInstance == null) {
            synchronized (CreateTaskRepository.class) {
                mInstance = new CreateTaskRepository();
            }
        }
        return  mInstance;
    }

    private CreateTaskRepository(){
    }

    public LiveData<List<CreateTaskAdapter.DataHolder>> getCreateTaskAdapterListLiveData() {
        return taskDetail;
    }

    public void loadCreateTaskAdapterList(Context context, List<CreateTaskAdapter.DataHolder> taskDetailList) {
        List<CreateTaskAdapter.DataHolder> dataHolders = new ArrayList<>();

        for(int i=0; i<taskDetailList.size(); i++){
            CreateTaskAdapter.DataHolder dataHolder = new CreateTaskAdapter.DataHolder();
            dataHolder.number = taskDetailList.get(i).number;
            dataHolder.title = taskDetailList.get(i).title;
            dataHolder.detail = taskDetailList.get(i).detail;

            dataHolders.add(dataHolder);
        }

        taskDetail.setValue(dataHolders);
    }

    public void addTask(Context context, MyTask inputTask) {
        Long taskID;
        DatabaseHelper.getInstance(context)
        .getMyTaskDao()
        .insert(inputTask);

        taskID = inputTask.getTaskId();

        if (inputTask.getReplaceCharactersWithNoDB().size() != 0){
            for(int i = 0; i < inputTask.getReplaceCharactersWithNoDB().size(); i++){
                inputTask.getReplaceCharactersWithNoDB().get(i).setTaskId(taskID);
                DatabaseHelper.getInstance(context)
                        .getReplaceCharactersDao()
                        .insert(inputTask.getReplaceCharacters().get(i));
            }
        }

        if (inputTask.getInsertCharactersWithNoDB().size() != 0){
            for(int i = 0; i < inputTask.getInsertCharactersWithNoDB().size(); i++){
                inputTask.getInsertCharactersWithNoDB().get(i).setTaskId(taskID);
                DatabaseHelper.getInstance(context)
                        .getInsertCharacterDao()
                        .insert(inputTask.getInsertCharacters().get(i));
            }
        }

        if (inputTask.getNewNameWithNoDB().size() != 0){
            for(int i = 0; i < inputTask.getNewNameWithNoDB().size(); i++){
                inputTask.getNewNameWithNoDB().get(i).setTaskId(taskID);
                DatabaseHelper.getInstance(context)
                        .getNewNameDao()
                        .insert(inputTask.getNewName().get(i));
            }
        }

        if (inputTask.getInsertNumbersWithNoDB().size() != 0){
            for(int i = 0; i < inputTask.getInsertNumbersWithNoDB().size(); i++){
                inputTask.getInsertNumbersWithNoDB().get(i).setTaskId(taskID);
                DatabaseHelper.getInstance(context)
                        .getInsertNumberDao()
                        .insert(inputTask.getInsertNumbers().get(i));
            }
        }

        if (inputTask.getInsertFileSizesWithNoDB().size() != 0){
            for(int i = 0; i < inputTask.getInsertFileSizesWithNoDB().size(); i++){
                inputTask.getInsertFileSizesWithNoDB().get(i).setTaskId(taskID);
                DatabaseHelper.getInstance(context)
                        .getInsertFileSizeDao()
                        .insert(inputTask.getInsertFileSizes().get(i));
            }
        }
        taskDetail.getValue().clear();
    }
}
