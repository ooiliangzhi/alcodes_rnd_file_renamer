package com.example.alcodes_rnd_file_renamer.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.alcodes_rnd_file_renamer.R;
import com.example.alcodes_rnd_file_renamer.adapters.DisplaySelectionTaskAdapter;
import com.example.alcodes_rnd_file_renamer.viewmodels.PageViewModel;
import com.example.alcodes_rnd_file_renamer.viewmodels.SelectFolderViewModel;
import com.example.alcodes_rnd_file_renamer.viewmodels.SelectFolderViewModelFactory;
import com.google.android.material.button.MaterialButton;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import timber.log.Timber;

public class SelectTaskFragment extends Fragment implements DisplaySelectionTaskAdapter.Callbacks {

    private static final String TAG = SelectTaskFragment.class.getSimpleName();

    private PageViewModel pageViewModel;

    private Unbinder mUnbinder;

    private DisplaySelectionTaskAdapter mAdapter;

    private SelectFolderViewModel mViewModel;

    @BindView(R.id.selection_task_item_recyclerview)
    protected RecyclerView mRecyclerView;

    @BindView(R.id.section_label_create_task)
    protected TextView labelCreateTask;

    @BindView(R.id.button_to_create_task)
    protected MaterialButton buttonCreateTask;

    boolean checkTaskSelected = false;

    public SelectTaskFragment() {
    }

    public static SelectTaskFragment newInstance() {
        return new SelectTaskFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        pageViewModel = ViewModelProviders.of(this).get(PageViewModel.class);
        pageViewModel.setIndex(TAG);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_select_task, container, false);

        final TextView textView = view.findViewById(R.id.section_label_create_task);

        textView.setText("No Task Had Created");

        mUnbinder = ButterKnife.bind(this, view);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        initView();
        initViewModel();
    }

    @Override
    public void onDestroy() {
        if (mUnbinder !=null){
            mUnbinder.unbind();
        }
        super.onDestroy();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void initView(){
        mAdapter = new DisplaySelectionTaskAdapter();
        mAdapter.setCallbacks(this);

        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), RecyclerView.VERTICAL, false));
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setAdapter(mAdapter);
    }

    public void initViewModel() {
        mViewModel = new ViewModelProvider(this, new SelectFolderViewModelFactory(getActivity().getApplication())).get(SelectFolderViewModel.class);
        mViewModel.getLoadDisplaySelectionTaskAdapterList().observe(getViewLifecycleOwner(), new Observer<List<DisplaySelectionTaskAdapter.DataHolder>>() {
            @Override
            public void onChanged(List<DisplaySelectionTaskAdapter.DataHolder> dataHolders) {
                mAdapter.setData(dataHolders);
                mAdapter.notifyDataSetChanged();

                if(dataHolders.isEmpty()){
                    buttonCreateTask.setVisibility(View.VISIBLE);
                    labelCreateTask.setVisibility(View.VISIBLE);
                    mRecyclerView.setVisibility(View.GONE);
                }
                else{
                    mRecyclerView.setVisibility(View.VISIBLE);
                    buttonCreateTask.setVisibility(View.GONE);
                    labelCreateTask.setVisibility(View.GONE);
                }
            }
        });
        mViewModel.loadDisplaySelectionTaskAdapterList();
    }

    @Override
    public void onListItemClicked(DisplaySelectionTaskAdapter.DataHolder data, AppCompatImageView imageView) {
        mViewModel.updateDisplaySelectionTaskAdapter(data);
        mAdapter.notifyDataSetChanged();
        checkTaskSelected = true;
    }
}