package com.example.alcodes_rnd_file_renamer.fragments;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.alcodes_rnd_file_renamer.R;
import com.example.alcodes_rnd_file_renamer.adapters.PreviewFolderAdapter;
import com.example.alcodes_rnd_file_renamer.viewmodels.PageViewModel;
import com.example.alcodes_rnd_file_renamer.viewmodels.SelectFolderViewModel;
import com.example.alcodes_rnd_file_renamer.viewmodels.SelectFolderViewModelFactory;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class PreviewFolderFragment extends Fragment {
    private static final String TAG = PreviewFolderFragment.class.getSimpleName();

    private PageViewModel pageViewModel;

    private Unbinder mUnbinder;

    private PreviewFolderAdapter mAdapter;

    private SelectFolderViewModel mViewModel;

    @BindView(R.id.rename_file_item_recyclerview)
    protected RecyclerView mRecyclerView;

    public PreviewFolderFragment() {
    }

    public static PreviewFolderFragment newInstance() {
        return new PreviewFolderFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        pageViewModel = ViewModelProviders.of(this).get(PageViewModel.class);
        pageViewModel.setIndex(TAG);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_preview_folder, container, false);

        mUnbinder = ButterKnife.bind(this, view);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        initView();
        initViewModel();
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            mViewModel.loadPreviewRenameFile();

        }
    }


    @Override
    public void onDestroy() {
        if (mUnbinder !=null){
            mUnbinder.unbind();
        }
        super.onDestroy();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void initView(){
        mAdapter = new PreviewFolderAdapter();

        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), RecyclerView.VERTICAL, false));
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setAdapter(mAdapter);
    }

    public void initViewModel() {
        mViewModel = new ViewModelProvider(this, new SelectFolderViewModelFactory(getActivity().getApplication())).get(SelectFolderViewModel.class);
        mViewModel.getLoadPreviewRenameFile().observe(getViewLifecycleOwner(), new Observer<List<PreviewFolderAdapter.DataHolder>>() {

            @Override
            public void onChanged(List<PreviewFolderAdapter.DataHolder> dataHolders) {
                mAdapter.setData(dataHolders);
                mAdapter.notifyDataSetChanged();
            }
        });
        mViewModel.loadPreviewRenameFile();
    }


    @OnClick(R.id.button_rename_file)
    public void startRenameFile(){
        if(mViewModel.getLoadDisplaySelectionTaskAdapterList().getValue().size() !=0){
            mViewModel.saveRenameFile();
            Toast.makeText(getActivity(), "File Renamed.", Toast.LENGTH_SHORT).show();
            getActivity().finish();
        }

    }
}
