package com.example.alcodes_rnd_file_renamer.fragments;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.MimeTypeMap;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.documentfile.provider.DocumentFile;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.alcodes_rnd_file_renamer.R;
import com.example.alcodes_rnd_file_renamer.adapters.SelectFolderAdapter;
import com.example.alcodes_rnd_file_renamer.viewmodels.PageViewModel;
import com.example.alcodes_rnd_file_renamer.viewmodels.SelectFolderViewModel;
import com.example.alcodes_rnd_file_renamer.viewmodels.SelectFolderViewModelFactory;
import com.google.android.material.button.MaterialButton;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class SelectFolderFragment extends Fragment implements SelectFolderAdapter.Callbacks {

    private static final String TAG = SelectFolderFragment.class.getSimpleName();

    //tabs Control
    private static final int OPEN_REQUEST_CODE = 100;

    private PageViewModel pageViewModel;

    //Content Control
    private Unbinder mUnbinder;

    private SelectFolderAdapter mAdapter;

    private List<SelectFolderAdapter.DataHolder> fileDetail = new ArrayList<>();

    @BindView(R.id.file_item_recyclerview)
    protected RecyclerView mRecyclerView;

    @BindView(R.id.section_label_select_folder)
    protected TextView labelSelectFolder;

    @BindView(R.id.button_select_folder)
    protected MaterialButton buttonSelectFolder;

    public SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm aaa");

    private boolean checkSelectAll = false;

    private Menu optionsMenu;

    private SelectFolderViewModel mViewModel;

    public SelectFolderFragment() {
    }

    public static SelectFolderFragment newInstance() {
        return new SelectFolderFragment();
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        inflater.inflate(R.menu.menu_fragment_select_file, menu);
        optionsMenu = menu;
        if (fileDetail.isEmpty()) {
            optionsMenu.setGroupVisible(0, false);
        }
    }

    @Override
    public void onPrepareOptionsMenu(@NonNull Menu menu) {
        super.onPrepareOptionsMenu(menu);

        if (checkSelectAll == false) {
            optionsMenu.getItem(0).setTitle("Select All");
            optionsMenu.getItem(0).setIcon(R.drawable.ic_deselect_all);
        } else {
            optionsMenu.getItem(0).setTitle("Deselect All");
            optionsMenu.getItem(0).setIcon(R.drawable.ic_select_all);
        }
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int itemId = item.getItemId();

        if (itemId == R.id.menu_select) {
            if (checkSelectAll) {
                checkSelectAll = false;
                mViewModel.updateSelectFolderAdapterList(checkSelectAll);
                item.setTitle("Select All");
                item.setIcon(R.drawable.ic_deselect_all);
            } else {
                checkSelectAll = true;
                mViewModel.updateSelectFolderAdapterList(checkSelectAll);
                item.setTitle("Deselect All");
                item.setIcon(R.drawable.ic_select_all);
            }
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        pageViewModel = ViewModelProviders.of(this).get(PageViewModel.class);
        pageViewModel.setIndex(TAG);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle SavedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_select_folder, container, false);

        mUnbinder = ButterKnife.bind(this, view);

        final TextView textView = view.findViewById(R.id.section_label_select_folder);

        textView.setText("No File Had Selected");

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        initView();
        initViewModel();
    }

    @Override
    public void onDestroy() {
        if (mUnbinder != null) {
            mUnbinder.unbind();
        }
        super.onDestroy();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        DocumentFile[] documentFiles;
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == OPEN_REQUEST_CODE) {
                if (data != null) {
                    documentFiles = DocumentFile.fromTreeUri(getActivity().getApplicationContext(), data.getData()).listFiles();
                    Uri fileUri;
                    String fileName;
                    String fileDate;
                    long fileSize;
                    ContentResolver cR = getContext().getContentResolver();
                    MimeTypeMap mime = MimeTypeMap.getSingleton();
                    String fileType;

                    for (int i = 0; i < documentFiles.length; i++) {
                        fileUri = documentFiles[i].getUri();
                        fileName = documentFiles[i].getName();
                        fileDate = dateFormat.format(new Date(documentFiles[i].lastModified()));
                        fileSize = documentFiles[i].length();
                        fileType = mime.getExtensionFromMimeType(cR.getType(documentFiles[i].getUri()));
                        SelectFolderAdapter.DataHolder dataHolder = new SelectFolderAdapter.DataHolder();
                        dataHolder.uri = fileUri;
                        dataHolder.fileName = fileName;
                        dataHolder.date = fileDate;
                        dataHolder.fileSize = fileSize;
                        dataHolder.fileType = fileType;
                        fileDetail.add(dataHolder);
                    }
                }
            }
        }
        mViewModel.loadSelectFolderAdapterList(fileDetail);

        if (fileDetail.isEmpty()) {
            optionsMenu.setGroupVisible(0, false);
        } else {
            optionsMenu.setGroupVisible(0, true);
        }
    }

    private void initView() {
        mAdapter = new SelectFolderAdapter();
        mAdapter.setCallbacks(this);

        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), RecyclerView.VERTICAL, false));
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setAdapter(mAdapter);
    }

    private void initViewModel() {
        mViewModel = new ViewModelProvider(this, new SelectFolderViewModelFactory(getActivity().getApplication())).get(SelectFolderViewModel.class);
        mViewModel.getSelectFolderAdapterListLiveData().observe(getViewLifecycleOwner(), new Observer<List<SelectFolderAdapter.DataHolder>>() {
            @Override
            public void onChanged(List<SelectFolderAdapter.DataHolder> fileDetailModels) {
                mAdapter.setData(fileDetailModels);
                mAdapter.notifyDataSetChanged();
                fileDetail = fileDetailModels;

                if (fileDetail.isEmpty()) {
                    buttonSelectFolder.setVisibility(View.VISIBLE);
                    labelSelectFolder.setVisibility(View.VISIBLE);
                    mRecyclerView.setVisibility(View.GONE);
                } else {
                    mRecyclerView.setVisibility(View.VISIBLE);
                    buttonSelectFolder.setVisibility(View.GONE);
                    labelSelectFolder.setVisibility(View.GONE);
                }
            }
        });
        mViewModel.loadSelectFolderAdapterList(fileDetail);
    }

    @OnClick(R.id.button_select_folder)
    public void openFolder() {
        Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT_TREE);
        startActivityForResult(intent, OPEN_REQUEST_CODE);
    }

    @Override
    public void onListItemClicked(SelectFolderAdapter.DataHolder data, AppCompatImageView imageView) {

        mViewModel.updateSingleSelectFolder(data);
        for (int i = 0; i < fileDetail.size(); i++) {
            if (fileDetail.get(i).check == false) {
                checkSelectAll = false;
                optionsMenu.getItem(0).setTitle("Select All");
                optionsMenu.getItem(0).setIcon(R.drawable.ic_deselect_all);
                break;
            } else {
                checkSelectAll = true;
                optionsMenu.getItem(0).setTitle("Deselect All");
                optionsMenu.getItem(0).setIcon(R.drawable.ic_select_all);
            }
        }
    }
}