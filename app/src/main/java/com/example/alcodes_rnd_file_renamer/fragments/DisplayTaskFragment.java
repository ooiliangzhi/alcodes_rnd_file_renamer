package com.example.alcodes_rnd_file_renamer.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.alcodes_rnd_file_renamer.R;
import com.example.alcodes_rnd_file_renamer.activities.EditTaskActivity;
import com.example.alcodes_rnd_file_renamer.adapters.DisplayTaskAdapter;
import com.example.alcodes_rnd_file_renamer.viewmodels.DisplayTaskViewModel;
import com.example.alcodes_rnd_file_renamer.viewmodels.DisplayTaskViewModelFactory;
import com.example.alcodes_rnd_file_renamer.viewmodels.ManageTaskPageViewModel;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class DisplayTaskFragment extends Fragment implements DisplayTaskAdapter.Callbacks {

    private static final String TAG = CreateTaskFragment.class.getSimpleName();

    private final int REQUEST_CODE_MY_TASK_DETAIL = 200;

    private ManageTaskPageViewModel manageTaskPageViewModel;

    private Unbinder mUnbinder;

    private DisplayTaskAdapter mAdapter;

    private DisplayTaskViewModel mViewModel;

    @BindView(R.id.task_item_recyclerview)
    protected RecyclerView mRecyclerView;

    public DisplayTaskFragment() {
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            mViewModel.loadDisplayTaskAdapterList();

        }
    }


    public static DisplayTaskFragment newInstance() {
        return new DisplayTaskFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        manageTaskPageViewModel = ViewModelProviders.of(this).get(ManageTaskPageViewModel.class);
        manageTaskPageViewModel.setIndex(TAG);
    }

    @Override
    public void onDestroy() {
        if (mUnbinder !=null){
            mUnbinder.unbind();
        }
        super.onDestroy();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState){
        View view = inflater.inflate(R.layout.fragment_display_task, container, false);

        mUnbinder = ButterKnife.bind(this, view);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        initView();
        initViewModel();
    }

    private void initView() {
        mAdapter = new DisplayTaskAdapter();
        mAdapter.setCallbacks(this);

        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), RecyclerView.VERTICAL, false));
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setAdapter(mAdapter);
    }

    private void initViewModel() {
        mViewModel = new ViewModelProvider(this, new DisplayTaskViewModelFactory(getActivity().getApplication())).get(DisplayTaskViewModel.class);
        mViewModel.getDisplayTaskAdapterListLiveData().observe(getViewLifecycleOwner(), new Observer<List<DisplayTaskAdapter.DataHolder>>() {
            @Override
            public void onChanged(List<DisplayTaskAdapter.DataHolder> taskModel) {
                mAdapter.setData(taskModel);
                mAdapter.notifyDataSetChanged();
            }
        });
        mViewModel.loadDisplayTaskAdapterList();
    }

    @Override
    public void onListItemClicked(DisplayTaskAdapter.DataHolder data) {
        Intent intent = new Intent(getActivity(), EditTaskActivity.class);
        intent.putExtra(EditTaskActivity.EXTRA_LONG_MY_TASK_ID, data.taskId);

        startActivityForResult(intent, REQUEST_CODE_MY_TASK_DETAIL);

    }

    @Override
    public void onDeleteButtonClicked(DisplayTaskAdapter.DataHolder data) {
        mViewModel.deleteTask(data.taskId);
    }
}
