package com.example.alcodes_rnd_file_renamer.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.alcodes_rnd_file_renamer.R;
import com.example.alcodes_rnd_file_renamer.activities.ManageTaskActivity;
import com.example.alcodes_rnd_file_renamer.activities.RenameFolderActivity;

import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class MainFragment extends Fragment {
    public static final String TAG = MainFragment.class.getSimpleName();

    private Unbinder mUnbinder;

    public MainFragment() {
    }

    public  static MainFragment newInstance() { return new MainFragment(); }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        inflater.inflate(R.menu.menu_fragment_main, menu);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState){
        View view = inflater.inflate(R.layout.fragment_main, container, false);

        mUnbinder = ButterKnife.bind(this, view);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @OnClick(R.id.button_select_folder)
    protected  void selectFolder() {
        startActivity(new Intent(getActivity(), RenameFolderActivity.class));
    }

    @OnClick(R.id.button_manage_rename_task)
    protected void manageRenameTask() {
        startActivity(new Intent(getActivity(), ManageTaskActivity.class));
    }

}
