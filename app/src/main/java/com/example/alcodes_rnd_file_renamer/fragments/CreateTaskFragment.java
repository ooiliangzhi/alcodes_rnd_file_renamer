package com.example.alcodes_rnd_file_renamer.fragments;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.alcodes_rnd_file_renamer.R;
import com.example.alcodes_rnd_file_renamer.adapters.CreateTaskAdapter;
import com.example.alcodes_rnd_file_renamer.database.entities.InsertCharacter;
import com.example.alcodes_rnd_file_renamer.database.entities.InsertFileSize;
import com.example.alcodes_rnd_file_renamer.database.entities.InsertNumber;
import com.example.alcodes_rnd_file_renamer.database.entities.MyTask;
import com.example.alcodes_rnd_file_renamer.database.entities.NewName;
import com.example.alcodes_rnd_file_renamer.database.entities.ReplaceCharacters;
import com.example.alcodes_rnd_file_renamer.viewmodels.CreateTaskViewModel;
import com.example.alcodes_rnd_file_renamer.viewmodels.CreateTaskViewModelFactory;
import com.example.alcodes_rnd_file_renamer.viewmodels.ManageTaskPageViewModel;
import com.google.android.material.checkbox.MaterialCheckBox;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.android.material.radiobutton.MaterialRadioButton;
import com.google.android.material.textfield.TextInputEditText;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class CreateTaskFragment extends Fragment implements CreateTaskAdapter.Callbacks{

    private static final String TAG = CreateTaskFragment.class.getSimpleName();

    private ManageTaskPageViewModel manageTaskPageViewModel;

    private Unbinder mUnbinder;

    private CreateTaskAdapter mAdapter;

    private List<CreateTaskAdapter.DataHolder> taskDetail = new ArrayList<>();

    private int numberCount = 1;

    private String[] criterionList = new String[]{};

    private CreateTaskViewModel mViewModel;

    @BindView(R.id.task_criterion_item_recyclerview)
    protected RecyclerView mRecyclerView;

    @BindView(R.id.edittext_task_name)
    protected TextInputEditText taskName;

    protected View layoutToDisplay;

    protected String titleToDisplay = "";

    protected MyTask myTask;

    protected int insertAt = 0;

    protected boolean checkAutoNumber = false;

    public CreateTaskFragment() {
    }

    public static CreateTaskFragment newInstance() {
        return new CreateTaskFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        manageTaskPageViewModel = ViewModelProviders.of(this).get(ManageTaskPageViewModel.class);
        manageTaskPageViewModel.setIndex(TAG);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_create_task, container, false);

        mUnbinder = ButterKnife.bind(this, view);
        criterionList = getActivity().getResources().getStringArray(R.array.criterion_item);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        initView();
        initViewModel();
    }

    @Override
    public void onDestroy() {
        if (mUnbinder !=null){
            mUnbinder.unbind();
        }
        super.onDestroy();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void initView(){
        mAdapter = new CreateTaskAdapter();
        mAdapter.setCallbacks(this);

        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), RecyclerView.VERTICAL, false));
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setAdapter(mAdapter);
        myTask = new MyTask("");
    }

    private void initViewModel() {
        mViewModel = new ViewModelProvider(this, new CreateTaskViewModelFactory(getActivity().getApplication())).get(CreateTaskViewModel.class);
        mViewModel.getCreateTaskAdapterListLiveData().observe(getViewLifecycleOwner(), new Observer<List<CreateTaskAdapter.DataHolder>>() {
            @Override
            public void onChanged(List<CreateTaskAdapter.DataHolder> taskDetailModel) {
                mAdapter.setData(taskDetailModel);
                mAdapter.notifyDataSetChanged();
                taskDetail = taskDetailModel;
            }
        });
        mViewModel.loadCreateTaskAdapterList(taskDetail);
    }

    @OnClick(R.id.button_create_task)
    public void createTask() {

        Date date = Calendar.getInstance().getTime();
        myTask.setTaskName(taskName.getText().toString());
        myTask.setTaskCreateDate(date);

        mViewModel.addTask(myTask);

        taskName.setText("");
        myTask = new MyTask("");
        numberCount = 1;

        mAdapter.notifyDataSetChanged();
        taskName.setEnabled(false);
        taskName.setEnabled(true);

        Toast.makeText(getActivity(), "Task Added", Toast.LENGTH_SHORT).show();
    }

    @OnClick(R.id.button_add_criterion)
    public void addCriterion() {
        final AlertDialog dialog = new MaterialAlertDialogBuilder(getActivity())
                .setTitle("SELECT CRITERION")
                .setView(R.layout.dialog_add_criterion)
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.cancel();
                    }
                })
                .show();
        ListView listView = (ListView) dialog.findViewById(R.id.listview);
        ArrayAdapter arrayAdapter = new ArrayAdapter(this.getActivity(),R.layout.item_list_criterion, R.id.textview_criterion, criterionList);
        listView.setAdapter(arrayAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                chooseCriterion(i);
                dialog.dismiss();
            }
        });
    }

    public void chooseCriterion(int position){
        RadioGroup radioGroup;
        MaterialCheckBox checkBox;
        switch (position){
            case 0 :
                layoutToDisplay = getLayoutInflater().inflate(R.layout.dialog_replace_characters,null);
                titleToDisplay = "Replace Characters";
                break;
            case 1 :
                layoutToDisplay = getLayoutInflater().inflate(R.layout.dialog_new_name,null);
                titleToDisplay = "New Name";
                break;
            case 2 :
                layoutToDisplay = getLayoutInflater().inflate(R.layout.dialog_insert_character_at,null);
                titleToDisplay = "Insert Character";

                radioGroup = layoutToDisplay.findViewById(R.id.radiogroup_insert_char_at);
                radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(RadioGroup radioGroup, int i) {
                        TextInputEditText inputText;
                        switch (i){
                            case R.id.radiobutton_begin:
                                insertAt = 0;
                                inputText = layoutToDisplay.findViewById(R.id.edittext_custom_position);
                                inputText.setVisibility(View.GONE);
                                break;
                            case R.id.radiobutton_end:
                                insertAt = 1;
                                inputText = layoutToDisplay.findViewById(R.id.edittext_custom_position);
                                inputText.setVisibility(View.GONE);
                                break;
                            case R.id.radiobutton_custom:
                                insertAt = 2;
                                inputText = layoutToDisplay.findViewById(R.id.edittext_custom_position);
                                inputText.setVisibility(View.VISIBLE);
                                break;
                        }
                    }
                });

                break;
            case 3 :
                layoutToDisplay = getLayoutInflater().inflate(R.layout.dialog_insert_number_at,null);
                titleToDisplay = "Insert Number";

                checkBox = layoutToDisplay.findViewById(R.id.checkbox_auto_number);
                checkBox.setOnCheckedChangeListener(new MaterialCheckBox.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                        RadioGroup autoNumRadio = layoutToDisplay.findViewById(R.id.radiogroup_auto_number);;
                        if(b == true){
                            autoNumRadio.setVisibility(View.VISIBLE);
                            checkAutoNumber = true;
                        }else{
                            autoNumRadio.setVisibility(View.GONE);
                            checkAutoNumber = false;
                        }
                    }
                });

                radioGroup = layoutToDisplay.findViewById(R.id.radiogroup_insert_num_at);
                radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(RadioGroup radioGroup, int i) {
                        TextInputEditText inputText;
                        switch (i){
                            case R.id.radiobutton_begin:
                                insertAt = 0;
                                inputText = layoutToDisplay.findViewById(R.id.edittext_custom_position);
                                inputText.setVisibility(View.GONE);
                                break;
                            case R.id.radiobutton_end:
                                insertAt = 1;
                                inputText = layoutToDisplay.findViewById(R.id.edittext_custom_position);
                                inputText.setVisibility(View.GONE);
                                break;
                            case R.id.radiobutton_custom:
                                insertAt = 2;
                                inputText = layoutToDisplay.findViewById(R.id.edittext_custom_position);
                                inputText.setVisibility(View.VISIBLE);
                                break;
                        }
                    }
                });

                break;
            case 4 :
                layoutToDisplay = getLayoutInflater().inflate(R.layout.dialog_insert_file_size,null);
                titleToDisplay = "Insert File Size";

                radioGroup = layoutToDisplay.findViewById(R.id.radiogroup_insert_file_size_at);
                radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(RadioGroup radioGroup, int i) {
                        TextInputEditText inputText;
                        switch (i){
                            case R.id.radiobutton_begin:
                                insertAt = 0;
                                inputText = layoutToDisplay.findViewById(R.id.edittext_custom_position);
                                inputText.setVisibility(View.GONE);
                                Log.e("t","s");
                                break;
                            case R.id.radiobutton_end:
                                insertAt = 1;
                                inputText = layoutToDisplay.findViewById(R.id.edittext_custom_position);
                                inputText.setVisibility(View.GONE);
                                break;
                            case R.id.radiobutton_custom:
                                insertAt = 2;
                                inputText = layoutToDisplay.findViewById(R.id.edittext_custom_position);
                                inputText.setVisibility(View.VISIBLE);
                                break;
                        }
                    }
                });

                break;
            default :
                layoutToDisplay = getLayoutInflater().inflate(null,null);
                titleToDisplay = "";
                break;
        }


        final AlertDialog innerDialog = new MaterialAlertDialogBuilder(getActivity())
                .setTitle(titleToDisplay)
                .setView(layoutToDisplay)
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.cancel();
                    }
                })
                .setPositiveButton("Add", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        getUserInputData(position);
                        dialogInterface.dismiss();
                    }
                })
                .show();
    }

    public void getUserInputData(int position){

        CreateTaskAdapter.DataHolder dataHolder = new CreateTaskAdapter.DataHolder();

        switch (position){
            case 0 :
                String matchCaseString;

                TextInputEditText charToReplace = layoutToDisplay.findViewById(R.id.edittext_char_to_replace);
                TextInputEditText replaceWith = layoutToDisplay.findViewById(R.id.edittext_replace_with);
                MaterialCheckBox matchCase = layoutToDisplay.findViewById(R.id.checkbox_match_case);

                myTask.getReplaceCharactersWithNoDB().add(new ReplaceCharacters(charToReplace.getText().toString(),
                        replaceWith.getText().toString(), matchCase.isChecked()));

                if(matchCase.isChecked()){
                    matchCaseString = "case sensitive";
                }else {
                    matchCaseString = "case insensitive";
                }


                dataHolder.number = numberCount;
                dataHolder.title = titleToDisplay;
                dataHolder.detail = "Replace \"" + charToReplace.getText().toString() + "\" with \"" +
                        replaceWith.getText().toString() + "\", " + matchCaseString;
                taskDetail.add(dataHolder);
                numberCount++;
                charToReplace.clearFocus();
                replaceWith.clearFocus();
                break;
            case 1 :
                TextInputEditText inputNewName = layoutToDisplay.findViewById(R.id.edittext_new_name);

                myTask.getNewNameWithNoDB().add(new NewName(inputNewName.getText().toString()));

                dataHolder.number = numberCount;
                dataHolder.title = titleToDisplay;
                dataHolder.detail = "Set name to \"" + inputNewName.getText().toString() +"\"";
                taskDetail.add(dataHolder);
                numberCount++;
                break;
            case 2 :
                String charInsertAtString = "";
                int charInsertPosition = 0;
                TextInputEditText charToInsert = layoutToDisplay.findViewById(R.id.edittext_char_to_insert);
                TextInputEditText customPositionInsert = layoutToDisplay.findViewById(R.id.edittext_custom_position);
                if(insertAt == 0){
                    charInsertAtString = "begin of name";
                    charInsertPosition = 0;
                }else if(insertAt == 1){
                    charInsertAtString = "end of name";
                    charInsertPosition = -1;
                }else if(insertAt == 2){
                    charInsertAtString = customPositionInsert.getText().toString();
                    charInsertPosition = Integer.parseInt(customPositionInsert.getText().toString());
                }

                myTask.getInsertCharactersWithNoDB().add(new InsertCharacter(charToInsert.getText().toString(), charInsertPosition));

                dataHolder.number = numberCount;
                dataHolder.title = titleToDisplay;
                dataHolder.detail = "Insert \"" + charToInsert.getText().toString() + "\" at position \"" + charInsertAtString + "\"";
                taskDetail.add(dataHolder);
                numberCount++;
                insertAt = 0;
                break;
            case 3 :
                String numInsertAtString = "";
                int numInsertPosition = 0;
                int autoMethod = 0;
                String autoMethodString = "";
                TextInputEditText numToInsert = layoutToDisplay.findViewById(R.id.edittext_num_to_insert);
                TextInputEditText customPositionInsertNum = layoutToDisplay.findViewById(R.id.edittext_custom_position);
                MaterialRadioButton increaseRadio = layoutToDisplay.findViewById(R.id.radiobutton_increase);
                MaterialRadioButton decreaseRadio = layoutToDisplay.findViewById(R.id.radiobutton_decrease);

                if(checkAutoNumber == true){
                    if(increaseRadio.isChecked()){
                        autoMethod = 1;
                        autoMethodString =" with increment";
                    }else if(decreaseRadio.isChecked()){
                        autoMethod = -1;
                        autoMethodString =" with decrement";
                    }
                }else{
                    autoMethod = 0;
                    autoMethodString = "";
                }

                if(insertAt == 0){
                    numInsertAtString = "begin of name";
                    numInsertPosition = 0;
                }else if(insertAt == 1){
                    numInsertAtString = "end of name";
                    numInsertPosition = -1;
                }else if(insertAt == 2){
                    numInsertAtString = customPositionInsertNum.getText().toString();
                    numInsertPosition = Integer.parseInt(customPositionInsertNum.getText().toString());
                }
                myTask.getInsertNumbersWithNoDB().add(new InsertNumber(numToInsert.getText().toString(), autoMethod, numInsertPosition));

                dataHolder.number = numberCount;
                dataHolder.title = titleToDisplay;
                dataHolder.detail = "Insert \"" + numToInsert.getText().toString() + "\" at position \"" + numInsertAtString + "\""+ autoMethodString;
                taskDetail.add(dataHolder);
                insertAt = 0;
                checkAutoNumber = false;
                numberCount++;
                break;
            case 4 :
                String fileSizeInsertAtString = "";
                int fileSizeInsertPosition = 0;

                Spinner formatSize = layoutToDisplay.findViewById(R.id.spinner_file_size);
                TextInputEditText decimalPlaceToInsert = layoutToDisplay.findViewById(R.id.edittext_decimal_place);
                TextInputEditText customPositionInsertFileSize = layoutToDisplay.findViewById(R.id.edittext_custom_position);

                if(insertAt == 0){
                    fileSizeInsertAtString = "begin of name";
                    fileSizeInsertPosition = 0;
                }else if(insertAt == 1){
                    fileSizeInsertAtString = "end of name";
                    fileSizeInsertPosition = -1;
                }else if(insertAt == 2){
                    fileSizeInsertAtString = customPositionInsertFileSize.getText().toString();
                    fileSizeInsertPosition = Integer.parseInt(customPositionInsertFileSize.getText().toString());
                }
                myTask.getInsertFileSizesWithNoDB().add(new InsertFileSize(Integer.parseInt(decimalPlaceToInsert.getText().toString()), formatSize.getSelectedItem().toString(), fileSizeInsertPosition));

                dataHolder.number = numberCount;
                dataHolder.title = titleToDisplay;
                dataHolder.detail = "Insert size with format \"" + formatSize.getSelectedItem().toString() + "\", \"" + decimalPlaceToInsert.getText().toString() + "\" d.p at position \""+ fileSizeInsertAtString + "\"";
                taskDetail.add(dataHolder);

                numberCount++;
                break;
            default :

                break;
        }

        mViewModel.loadCreateTaskAdapterList(taskDetail);


    }

    @Override
    public void onListItemClicked(CreateTaskAdapter.DataHolder data) {
        //TODO future
    }

}
