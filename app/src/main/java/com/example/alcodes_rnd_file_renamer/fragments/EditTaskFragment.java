package com.example.alcodes_rnd_file_renamer.fragments;

import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.alcodes_rnd_file_renamer.R;
import com.example.alcodes_rnd_file_renamer.activities.EditTaskActivity;
import com.example.alcodes_rnd_file_renamer.adapters.DisplayTaskAdapter;
import com.example.alcodes_rnd_file_renamer.adapters.EditTaskAdapter;
import com.example.alcodes_rnd_file_renamer.database.entities.InsertCharacter;
import com.example.alcodes_rnd_file_renamer.database.entities.InsertFileSize;
import com.example.alcodes_rnd_file_renamer.database.entities.InsertNumber;
import com.example.alcodes_rnd_file_renamer.database.entities.MyTask;
import com.example.alcodes_rnd_file_renamer.database.entities.NewName;
import com.example.alcodes_rnd_file_renamer.database.entities.ReplaceCharacters;
import com.example.alcodes_rnd_file_renamer.viewmodels.DisplayTaskViewModel;
import com.example.alcodes_rnd_file_renamer.viewmodels.DisplayTaskViewModelFactory;
import com.google.android.material.checkbox.MaterialCheckBox;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.android.material.radiobutton.MaterialRadioButton;
import com.google.android.material.textfield.TextInputEditText;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class EditTaskFragment extends Fragment implements EditTaskAdapter.Callbacks {

    public static final String TAG = EditTaskFragment.class.getSimpleName();

    private static final String ARG_LONG_MY_TASK_ID = "ARG_LONG_MY_TASK_ID";

    private DisplayTaskViewModel mViewModel;

    private EditTaskAdapter mAdapter;

    @BindView(R.id.edit_task_criterion_item_recyclerview)
    protected RecyclerView mRecyclerView;

    @BindView(R.id.edittext_edit_task_name)
    protected TextInputEditText editTaskName;

    private Unbinder mUnbinder;
    private long mMyTaskId = 0L;

    private String[] criterionList = new String[]{};

    protected View layoutToDisplay;

    protected String titleToDisplay = "";

    protected MyTask myTask;

    protected int insertAt = 0;

    protected boolean checkAutoNumber = false;

    protected int numberCount = 0;

    public EditTaskFragment() {
    }

    public static EditTaskFragment newInstance(long id) {
        Bundle args = new Bundle();
        args.putLong(ARG_LONG_MY_TASK_ID, id);

        EditTaskFragment fragment = new EditTaskFragment();
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_edit_task, container, false);

        mUnbinder = ButterKnife.bind(this, view);
        criterionList = getActivity().getResources().getStringArray(R.array.criterion_item);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        // Check arguments pass from previous page.
        Bundle args = getArguments();

        if (args != null) {
            mMyTaskId = args.getLong(ARG_LONG_MY_TASK_ID, 0);
        }

        initView();
        initViewModel();
    }

    @Override
    public void onDestroy() {
        if (mUnbinder != null) {
            mUnbinder.unbind();
        }

        super.onDestroy();
    }

    private void initView(){
        mAdapter = new EditTaskAdapter();
        mAdapter.setCallbacks(this);

        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), RecyclerView.VERTICAL, false));
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setAdapter(mAdapter);
    }

    public void initViewModel() {
        if (mMyTaskId > 0) {
            mViewModel = new ViewModelProvider(this, new DisplayTaskViewModelFactory(getActivity().getApplication())).get(DisplayTaskViewModel.class);
            mViewModel.getLoadSingleTask().observe(getViewLifecycleOwner(), new Observer<MyTask>() {
                @Override
                public void onChanged(MyTask mytask) {
                    if (mytask != null) {
                        editTaskName.setText(mytask.getTaskName().toString());
                        mAdapter.setData(mViewModel.getLoadSingleTaskCriterion().getValue());
                        mAdapter.notifyDataSetChanged();

                    } else {
                        // Record not found.
                        Toast.makeText(getActivity(), "No task found.", Toast.LENGTH_SHORT).show();
                        getActivity().finish();
                    }
                }
            });

            mViewModel.loadSingleTask(mMyTaskId);
        }
    }

    @OnClick(R.id.button_save)
    public void updateTask() {
        mViewModel.updateTaskName(editTaskName.getText().toString(), mMyTaskId);

        getActivity().setResult(EditTaskActivity.RESULT_CONTENT_MODIFIED);
        getActivity().finish();
    }

    @OnClick(R.id.button_add_criterion)
    public void addCriterion() {
        final AlertDialog dialog = new MaterialAlertDialogBuilder(getActivity())
                .setTitle("SELECT CRITERION")
                .setView(R.layout.dialog_add_criterion)
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.cancel();
                    }
                })
                .show();
        ListView listView = (ListView) dialog.findViewById(R.id.listview);
        ArrayAdapter arrayAdapter = new ArrayAdapter(this.getActivity(),R.layout.item_list_criterion, R.id.textview_criterion, criterionList);
        listView.setAdapter(arrayAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                chooseCriterion(i);
                dialog.dismiss();
            }
        });
    }

    public void chooseCriterion(int position){
        RadioGroup radioGroup;
        MaterialCheckBox checkBox;
        switch (position){
            case 0 :
                layoutToDisplay = getLayoutInflater().inflate(R.layout.dialog_replace_characters,null);
                titleToDisplay = "Replace Characters";
                break;
            case 1 :
                layoutToDisplay = getLayoutInflater().inflate(R.layout.dialog_new_name,null);
                titleToDisplay = "New Name";
                break;
            case 2 :
                layoutToDisplay = getLayoutInflater().inflate(R.layout.dialog_insert_character_at,null);
                titleToDisplay = "Insert Character At";

                radioGroup = layoutToDisplay.findViewById(R.id.radiogroup_insert_char_at);
                radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(RadioGroup radioGroup, int i) {
                        TextInputEditText inputText;
                        switch (i){
                            case R.id.radiobutton_begin:
                                insertAt = 0;
                                inputText = layoutToDisplay.findViewById(R.id.edittext_custom_position);
                                inputText.setVisibility(View.GONE);
                                break;
                            case R.id.radiobutton_end:
                                insertAt = 1;
                                inputText = layoutToDisplay.findViewById(R.id.edittext_custom_position);
                                inputText.setVisibility(View.GONE);
                                break;
                            case R.id.radiobutton_custom:
                                insertAt = 2;
                                inputText = layoutToDisplay.findViewById(R.id.edittext_custom_position);
                                inputText.setVisibility(View.VISIBLE);
                                break;
                        }
                    }
                });

                break;
            case 3 :
                layoutToDisplay = getLayoutInflater().inflate(R.layout.dialog_insert_number_at,null);
                titleToDisplay = "Insert Number At";

                checkBox = layoutToDisplay.findViewById(R.id.checkbox_auto_number);
                checkBox.setOnCheckedChangeListener(new MaterialCheckBox.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                        RadioGroup autoNumRadio = layoutToDisplay.findViewById(R.id.radiogroup_auto_number);;
                        if(b == true){
                            autoNumRadio.setVisibility(View.VISIBLE);
                            checkAutoNumber = true;
                        }else{
                            autoNumRadio.setVisibility(View.GONE);
                            checkAutoNumber = false;
                        }
                    }
                });

                radioGroup = layoutToDisplay.findViewById(R.id.radiogroup_insert_num_at);
                radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(RadioGroup radioGroup, int i) {
                        TextInputEditText inputText;
                        switch (i){
                            case R.id.radiobutton_begin:
                                insertAt = 0;
                                inputText = layoutToDisplay.findViewById(R.id.edittext_custom_position);
                                inputText.setVisibility(View.GONE);
                                break;
                            case R.id.radiobutton_end:
                                insertAt = 1;
                                inputText = layoutToDisplay.findViewById(R.id.edittext_custom_position);
                                inputText.setVisibility(View.GONE);
                                break;
                            case R.id.radiobutton_custom:
                                insertAt = 2;
                                inputText = layoutToDisplay.findViewById(R.id.edittext_custom_position);
                                inputText.setVisibility(View.VISIBLE);
                                break;
                        }
                    }
                });

                break;
            case 4 :
                layoutToDisplay = getLayoutInflater().inflate(R.layout.dialog_insert_file_size,null);
                titleToDisplay = "Insert File Size";

                radioGroup = layoutToDisplay.findViewById(R.id.radiogroup_insert_file_size_at);
                radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(RadioGroup radioGroup, int i) {
                        TextInputEditText inputText;
                        switch (i){
                            case R.id.radiobutton_begin:
                                insertAt = 0;
                                inputText = layoutToDisplay.findViewById(R.id.edittext_custom_position);
                                inputText.setVisibility(View.GONE);
                                break;
                            case R.id.radiobutton_end:
                                insertAt = 1;
                                inputText = layoutToDisplay.findViewById(R.id.edittext_custom_position);
                                inputText.setVisibility(View.GONE);
                                break;
                            case R.id.radiobutton_custom:
                                insertAt = 2;
                                inputText = layoutToDisplay.findViewById(R.id.edittext_custom_position);
                                inputText.setVisibility(View.VISIBLE);
                                break;
                        }
                    }
                });

                break;
            default :
                layoutToDisplay = getLayoutInflater().inflate(null,null);
                titleToDisplay = "";
                break;
        }


        final AlertDialog innerDialog = new MaterialAlertDialogBuilder(getActivity())
                .setTitle(titleToDisplay)
                .setView(layoutToDisplay)
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.cancel();
                    }
                })
                .setPositiveButton("Add", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        getUserInputData(position);
                        mAdapter.setData(mViewModel.getLoadSingleTaskCriterion().getValue());
                        mAdapter.notifyDataSetChanged();
                        dialogInterface.dismiss();
                    }
                })
                .show();
    }

    public void getUserInputData(int position){

        EditTaskAdapter.DataHolder dataHolder = new EditTaskAdapter.DataHolder();

        switch (position){
            case 0 :
                String matchCaseString;

                TextInputEditText charToReplace = layoutToDisplay.findViewById(R.id.edittext_char_to_replace);
                TextInputEditText replaceWith = layoutToDisplay.findViewById(R.id.edittext_replace_with);
                MaterialCheckBox matchCase = layoutToDisplay.findViewById(R.id.checkbox_match_case);

                ReplaceCharacters replaceCharacters = new ReplaceCharacters(charToReplace.getText().toString(),
                        replaceWith.getText().toString(), matchCase.isChecked());

                mViewModel.addReplaceCharacters(replaceCharacters, mMyTaskId);

                break;
            case 1 :
                TextInputEditText inputNewName = layoutToDisplay.findViewById(R.id.edittext_new_name);

                NewName newName = new NewName(inputNewName.getText().toString());
                mViewModel.addNewName(newName, mMyTaskId);


                break;
            case 2 :
                int charInsertPosition = 0;
                TextInputEditText charToInsert = layoutToDisplay.findViewById(R.id.edittext_char_to_insert);
                TextInputEditText customPositionInsert = layoutToDisplay.findViewById(R.id.edittext_custom_position);
                if(insertAt == 0){
                    charInsertPosition = 0;
                }else if(insertAt == 1){
                    charInsertPosition = -1;
                }else if(insertAt == 2){
                    charInsertPosition = Integer.parseInt(customPositionInsert.getText().toString());
                }

                InsertCharacter insertCharacter = new InsertCharacter(charToInsert.getText().toString(), charInsertPosition);
                mViewModel.addInsertCharacter(insertCharacter, mMyTaskId);

                insertAt = 0;
                break;
            case 3 :
                int numInsertPosition = 0;
                int autoMethod = 0;
                TextInputEditText numToInsert = layoutToDisplay.findViewById(R.id.edittext_num_to_insert);
                TextInputEditText customPositionInsertNum = layoutToDisplay.findViewById(R.id.edittext_custom_position);
                MaterialRadioButton increaseRadio = layoutToDisplay.findViewById(R.id.radiobutton_increase);
                MaterialRadioButton decreaseRadio = layoutToDisplay.findViewById(R.id.radiobutton_decrease);

                if(checkAutoNumber == true){
                    if(increaseRadio.isChecked()){
                        autoMethod = 1;
                    }else if(decreaseRadio.isChecked()){
                        autoMethod = -1;
                    }
                }else{
                    autoMethod = 0;
                }

                if(insertAt == 0){
                    numInsertPosition = 0;
                }else if(insertAt == 1){
                    numInsertPosition = -1;
                }else if(insertAt == 2){
                    numInsertPosition = Integer.parseInt(customPositionInsertNum.getText().toString());
                }
                InsertNumber insertNumber = new InsertNumber(numToInsert.getText().toString(), autoMethod, numInsertPosition);

                mViewModel.addInsertNumber(insertNumber, mMyTaskId);

                insertAt = 0;
                checkAutoNumber = false;
                numberCount++;
                break;
            case 4 :
                int fileSizeInsertPosition = 0;

                Spinner formatSize = layoutToDisplay.findViewById(R.id.spinner_file_size);
                TextInputEditText decimalPlaceToInsert = layoutToDisplay.findViewById(R.id.edittext_decimal_place);
                TextInputEditText customPositionInsertFileSize = layoutToDisplay.findViewById(R.id.edittext_custom_position);

                if(insertAt == 0){
                    fileSizeInsertPosition = 0;
                }else if(insertAt == 1){
                    fileSizeInsertPosition = -1;
                }else if(insertAt == 2){
                    fileSizeInsertPosition = Integer.parseInt(customPositionInsertFileSize.getText().toString());
                }
                InsertFileSize insertFileSize = new InsertFileSize(Integer.parseInt(decimalPlaceToInsert.getText().toString()), formatSize.getSelectedItem().toString(), fileSizeInsertPosition);
                mViewModel.addInsertFileSize(insertFileSize, mMyTaskId);

                break;
            default :

                break;
        }
    }

    @Override
    public void onDeleteButtonClicked(EditTaskAdapter.DataHolder data) {
        mViewModel.deleteCriterion(data, mMyTaskId);
        mAdapter.setData(mViewModel.getLoadSingleTaskCriterion().getValue());
        mAdapter.notifyDataSetChanged();
    }

}
