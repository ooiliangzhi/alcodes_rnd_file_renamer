package com.example.alcodes_rnd_file_renamer.database.entities;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.NotNull;
import org.greenrobot.greendao.annotation.Generated;

@Entity
public class InsertFileSize {

    @Id(autoincrement = true)
    private Long insertFileSizeId;

    @NotNull
    private Long taskId;

    @NotNull
    private int decimalPlace;

    @NotNull
    private String formatSize;

    @NotNull
    private int insertAt;

    public InsertFileSize(int decimalPlace, String formatSize, int insertAt) {
        this.decimalPlace = decimalPlace;
        this.formatSize = formatSize;
        this.insertAt = insertAt;
    }

    @Generated(hash = 247426185)
    public InsertFileSize(Long insertFileSizeId, @NotNull Long taskId,
            int decimalPlace, @NotNull String formatSize, int insertAt) {
        this.insertFileSizeId = insertFileSizeId;
        this.taskId = taskId;
        this.decimalPlace = decimalPlace;
        this.formatSize = formatSize;
        this.insertAt = insertAt;
    }

    @Generated(hash = 1846507418)
    public InsertFileSize() {
    }

    public Long getInsertFileSizeId() {
        return this.insertFileSizeId;
    }

    public void setInsertFileSizeId(Long insertFileSizeId) {
        this.insertFileSizeId = insertFileSizeId;
    }

    public Long getTaskId() {
        return this.taskId;
    }

    public void setTaskId(Long taskId) {
        this.taskId = taskId;
    }

    public int getDecimalPlace() {
        return this.decimalPlace;
    }

    public void setDecimalPlace(int decimalPlace) {
        this.decimalPlace = decimalPlace;
    }

    public String getFormatSize() {
        return this.formatSize;
    }

    public void setFormatSize(String formatSize) {
        this.formatSize = formatSize;
    }

    public int getInsertAt() {
        return this.insertAt;
    }

    public void setInsertAt(int insertAt) {
        this.insertAt = insertAt;
    }



}
