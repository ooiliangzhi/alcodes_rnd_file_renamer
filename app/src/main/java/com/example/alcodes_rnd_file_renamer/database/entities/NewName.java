package com.example.alcodes_rnd_file_renamer.database.entities;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.NotNull;
import org.greenrobot.greendao.annotation.Generated;

@Entity
public class NewName {
    @Id(autoincrement = true)
    private Long newNameId;

    @NotNull
    private Long taskId;

    @NotNull
    private String newName;

    public NewName(String newName) {
        this.newName = newName;
    }

    @Generated(hash = 392113752)
    public NewName(Long newNameId, @NotNull Long taskId, @NotNull String newName) {
        this.newNameId = newNameId;
        this.taskId = taskId;
        this.newName = newName;
    }

    @Generated(hash = 991206095)
    public NewName() {
    }

    public Long getNewNameId() {
        return this.newNameId;
    }

    public void setNewNameId(Long newNameId) {
        this.newNameId = newNameId;
    }

    public Long getTaskId() {
        return this.taskId;
    }

    public void setTaskId(Long taskId) {
        this.taskId = taskId;
    }

    public String getNewName() {
        return this.newName;
    }

    public void setNewName(String newName) {
        this.newName = newName;
    }

}
