package com.example.alcodes_rnd_file_renamer.database.entities;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.NotNull;
import org.greenrobot.greendao.annotation.Generated;

@Entity
public class InsertNumber {
    @Id(autoincrement = true)
    private Long insertNumberId;

    @NotNull
    private Long taskId;

    @NotNull
    private String numberToInsert;

    @NotNull
    private int autoNumber;

    @NotNull
    private int insertAt;

    public InsertNumber(String numberToInsert, int autoNumber, int insertAt){
        this.numberToInsert = numberToInsert;
        this.autoNumber = autoNumber;
        this.insertAt = insertAt;
    }

    @Generated(hash = 2058803360)
    public InsertNumber(Long insertNumberId, @NotNull Long taskId,
            @NotNull String numberToInsert, int autoNumber, int insertAt) {
        this.insertNumberId = insertNumberId;
        this.taskId = taskId;
        this.numberToInsert = numberToInsert;
        this.autoNumber = autoNumber;
        this.insertAt = insertAt;
    }

    @Generated(hash = 1787458044)
    public InsertNumber() {
    }

    public Long getInsertNumberId() {
        return this.insertNumberId;
    }

    public void setInsertNumberId(Long insertNumberId) {
        this.insertNumberId = insertNumberId;
    }

    public Long getTaskId() {
        return this.taskId;
    }

    public void setTaskId(Long taskId) {
        this.taskId = taskId;
    }

    public String getNumberToInsert() {
        return this.numberToInsert;
    }

    public void setNumberToInsert(String numberToInsert) {
        this.numberToInsert = numberToInsert;
    }

    public int getAutoNumber() {
        return this.autoNumber;
    }

    public void setAutoNumber(int autoNumber) {
        this.autoNumber = autoNumber;
    }

    public int getInsertAt() {
        return this.insertAt;
    }

    public void setInsertAt(int insertAt) {
        this.insertAt = insertAt;
    }

}
