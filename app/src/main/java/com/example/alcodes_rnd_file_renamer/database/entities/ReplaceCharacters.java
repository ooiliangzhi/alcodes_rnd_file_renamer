package com.example.alcodes_rnd_file_renamer.database.entities;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.NotNull;
import org.greenrobot.greendao.annotation.Generated;

@Entity
public class ReplaceCharacters {

    @Id(autoincrement = true)
    private Long replaceCharacterId;

    @NotNull
    private Long taskId;

    @NotNull
    private String characterToReplace;

    @NotNull
    private String replaceWith;

    @NotNull
    private boolean matchCase;

    public ReplaceCharacters(String characterToReplace, String replaceWith, boolean matchCase) {
        this.characterToReplace = characterToReplace;
        this.replaceWith = replaceWith;
        this.matchCase = matchCase;
    }

    @Generated(hash = 330684035)
    public ReplaceCharacters(Long replaceCharacterId, @NotNull Long taskId,
            @NotNull String characterToReplace, @NotNull String replaceWith, boolean matchCase) {
        this.replaceCharacterId = replaceCharacterId;
        this.taskId = taskId;
        this.characterToReplace = characterToReplace;
        this.replaceWith = replaceWith;
        this.matchCase = matchCase;
    }

    @Generated(hash = 2124669619)
    public ReplaceCharacters() {
    }

    public Long getReplaceCharacterId() {
        return this.replaceCharacterId;
    }

    public void setReplaceCharacterId(Long replaceCharacterId) {
        this.replaceCharacterId = replaceCharacterId;
    }

    public Long getTaskId() {
        return this.taskId;
    }

    public void setTaskId(Long taskId) {
        this.taskId = taskId;
    }

    public String getCharacterToReplace() {
        return this.characterToReplace;
    }

    public void setCharacterToReplace(String characterToReplace) {
        this.characterToReplace = characterToReplace;
    }

    public String getReplaceWith() {
        return this.replaceWith;
    }

    public void setReplaceWith(String replaceWith) {
        this.replaceWith = replaceWith;
    }

    public boolean getMatchCase() {
        return this.matchCase;
    }

    public void setMatchCase(boolean matchCase) {
        this.matchCase = matchCase;
    }

}
