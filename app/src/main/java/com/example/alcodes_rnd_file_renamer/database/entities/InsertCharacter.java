package com.example.alcodes_rnd_file_renamer.database.entities;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.NotNull;
import org.greenrobot.greendao.annotation.Generated;

@Entity
public class InsertCharacter {

    @Id(autoincrement = true)
    private Long insertCharacterId;

    @NotNull
    private Long taskId;

    @NotNull
    private String characterToInsert;

    @NotNull
    private int insertAt;

    public InsertCharacter(String characterToInsert, int insertAt){
        this.characterToInsert = characterToInsert;
        this.insertAt = insertAt;
    }

    @Generated(hash = 322200772)
    public InsertCharacter(Long insertCharacterId, @NotNull Long taskId,
            @NotNull String characterToInsert, int insertAt) {
        this.insertCharacterId = insertCharacterId;
        this.taskId = taskId;
        this.characterToInsert = characterToInsert;
        this.insertAt = insertAt;
    }

    @Generated(hash = 1436784)
    public InsertCharacter() {
    }

    public Long getInsertCharacterId() {
        return this.insertCharacterId;
    }

    public void setInsertCharacterId(Long insertCharacterId) {
        this.insertCharacterId = insertCharacterId;
    }

    public Long getTaskId() {
        return this.taskId;
    }

    public void setTaskId(Long taskId) {
        this.taskId = taskId;
    }

    public String getCharacterToInsert() {
        return this.characterToInsert;
    }

    public void setCharacterToInsert(String characterToInsert) {
        this.characterToInsert = characterToInsert;
    }

    public int getInsertAt() {
        return this.insertAt;
    }

    public void setInsertAt(int insertAt) {
        this.insertAt = insertAt;
    }

}
