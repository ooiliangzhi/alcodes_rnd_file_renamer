package com.example.alcodes_rnd_file_renamer.database.entities;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.NotNull;
import org.greenrobot.greendao.annotation.ToMany;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.DaoException;

@Entity
public class MyTask {

    @Id(autoincrement = true)
    private Long taskId;

    @NotNull
    private String taskName;

    @NotNull
    private Date taskCreateDate;

    @ToMany(referencedJoinProperty = "taskId")
    private List<ReplaceCharacters> replaceCharacters;

    @ToMany(referencedJoinProperty = "taskId")
    private List<NewName> newName;

    @ToMany(referencedJoinProperty = "taskId")
    private List<InsertCharacter> insertCharacters;

    @ToMany(referencedJoinProperty = "taskId")
    private List<InsertNumber> insertNumbers;

    @ToMany(referencedJoinProperty = "taskId")
    private List<InsertFileSize> insertFileSizes;

    /** Used to resolve relations */
    @Generated(hash = 2040040024)
    private transient DaoSession daoSession;

    /** Used for active entity operations. */
    @Generated(hash = 1840562355)
    private transient MyTaskDao myDao;

    public MyTask(String taskName){
        this.replaceCharacters = new ArrayList<>();
        this.newName = new ArrayList<>();
        this.insertCharacters = new ArrayList<>();
        this.insertNumbers = new ArrayList<>();
        this.insertFileSizes = new ArrayList<>();
    }

    @Generated(hash = 1021383394)
    public MyTask(Long taskId, @NotNull String taskName,
            @NotNull Date taskCreateDate) {
        this.taskId = taskId;
        this.taskName = taskName;
        this.taskCreateDate = taskCreateDate;
    }

    @Generated(hash = 1229326037)
    public MyTask() {
    }

    public List<ReplaceCharacters> getReplaceCharactersWithNoDB(){
        return replaceCharacters;
    }

    public List<NewName> getNewNameWithNoDB() {
        return newName;
    }

    public List<InsertCharacter> getInsertCharactersWithNoDB() {
        return insertCharacters;
    }

    public List<InsertNumber> getInsertNumbersWithNoDB() {
        return insertNumbers;
    }

    public List<InsertFileSize> getInsertFileSizesWithNoDB () {
        return insertFileSizes;
    }

    public Long getTaskId() {
        return this.taskId;
    }

    public void setTaskId(Long taskId) {
        this.taskId = taskId;
    }

    public String getTaskName() {
        return this.taskName;
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    public Date getTaskCreateDate() {
        return this.taskCreateDate;
    }

    public void setTaskCreateDate(Date taskCreateDate) {
        this.taskCreateDate = taskCreateDate;
    }

    /**
     * To-many relationship, resolved on first access (and after reset).
     * Changes to to-many relations are not persisted, make changes to the target entity.
     */
    @Generated(hash = 334449209)
    public List<ReplaceCharacters> getReplaceCharacters() {
        if (replaceCharacters == null) {
            final DaoSession daoSession = this.daoSession;
            if (daoSession == null) {
                throw new DaoException("Entity is detached from DAO context");
            }
            ReplaceCharactersDao targetDao = daoSession.getReplaceCharactersDao();
            List<ReplaceCharacters> replaceCharactersNew = targetDao
                    ._queryMyTask_ReplaceCharacters(taskId);
            synchronized (this) {
                if (replaceCharacters == null) {
                    replaceCharacters = replaceCharactersNew;
                }
            }
        }
        return replaceCharacters;
    }

    /** Resets a to-many relationship, making the next get call to query for a fresh result. */
    @Generated(hash = 548848018)
    public synchronized void resetReplaceCharacters() {
        replaceCharacters = null;
    }

    /**
     * To-many relationship, resolved on first access (and after reset).
     * Changes to to-many relations are not persisted, make changes to the target entity.
     */
    @Generated(hash = 1673941714)
    public List<NewName> getNewName() {
        if (newName == null) {
            final DaoSession daoSession = this.daoSession;
            if (daoSession == null) {
                throw new DaoException("Entity is detached from DAO context");
            }
            NewNameDao targetDao = daoSession.getNewNameDao();
            List<NewName> newNameNew = targetDao._queryMyTask_NewName(taskId);
            synchronized (this) {
                if (newName == null) {
                    newName = newNameNew;
                }
            }
        }
        return newName;
    }

    /** Resets a to-many relationship, making the next get call to query for a fresh result. */
    @Generated(hash = 107598985)
    public synchronized void resetNewName() {
        newName = null;
    }

    /**
     * To-many relationship, resolved on first access (and after reset).
     * Changes to to-many relations are not persisted, make changes to the target entity.
     */
    @Generated(hash = 1192497052)
    public List<InsertCharacter> getInsertCharacters() {
        if (insertCharacters == null) {
            final DaoSession daoSession = this.daoSession;
            if (daoSession == null) {
                throw new DaoException("Entity is detached from DAO context");
            }
            InsertCharacterDao targetDao = daoSession.getInsertCharacterDao();
            List<InsertCharacter> insertCharactersNew = targetDao
                    ._queryMyTask_InsertCharacters(taskId);
            synchronized (this) {
                if (insertCharacters == null) {
                    insertCharacters = insertCharactersNew;
                }
            }
        }
        return insertCharacters;
    }

    /** Resets a to-many relationship, making the next get call to query for a fresh result. */
    @Generated(hash = 1256595669)
    public synchronized void resetInsertCharacters() {
        insertCharacters = null;
    }

    /**
     * To-many relationship, resolved on first access (and after reset).
     * Changes to to-many relations are not persisted, make changes to the target entity.
     */
    @Generated(hash = 290280975)
    public List<InsertNumber> getInsertNumbers() {
        if (insertNumbers == null) {
            final DaoSession daoSession = this.daoSession;
            if (daoSession == null) {
                throw new DaoException("Entity is detached from DAO context");
            }
            InsertNumberDao targetDao = daoSession.getInsertNumberDao();
            List<InsertNumber> insertNumbersNew = targetDao
                    ._queryMyTask_InsertNumbers(taskId);
            synchronized (this) {
                if (insertNumbers == null) {
                    insertNumbers = insertNumbersNew;
                }
            }
        }
        return insertNumbers;
    }

    /** Resets a to-many relationship, making the next get call to query for a fresh result. */
    @Generated(hash = 205562288)
    public synchronized void resetInsertNumbers() {
        insertNumbers = null;
    }

    /**
     * To-many relationship, resolved on first access (and after reset).
     * Changes to to-many relations are not persisted, make changes to the target entity.
     */
    @Generated(hash = 70411190)
    public List<InsertFileSize> getInsertFileSizes() {
        if (insertFileSizes == null) {
            final DaoSession daoSession = this.daoSession;
            if (daoSession == null) {
                throw new DaoException("Entity is detached from DAO context");
            }
            InsertFileSizeDao targetDao = daoSession.getInsertFileSizeDao();
            List<InsertFileSize> insertFileSizesNew = targetDao
                    ._queryMyTask_InsertFileSizes(taskId);
            synchronized (this) {
                if (insertFileSizes == null) {
                    insertFileSizes = insertFileSizesNew;
                }
            }
        }
        return insertFileSizes;
    }

    /** Resets a to-many relationship, making the next get call to query for a fresh result. */
    @Generated(hash = 524888736)
    public synchronized void resetInsertFileSizes() {
        insertFileSizes = null;
    }

    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#delete(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 128553479)
    public void delete() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.delete(this);
    }

    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#refresh(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 1942392019)
    public void refresh() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.refresh(this);
    }

    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#update(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 713229351)
    public void update() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.update(this);
    }

    /** called by internal mechanisms, do not call yourself. */
    @Generated(hash = 226883707)
    public void __setDaoSession(DaoSession daoSession) {
        this.daoSession = daoSession;
        myDao = daoSession != null ? daoSession.getMyTaskDao() : null;
    }


}
