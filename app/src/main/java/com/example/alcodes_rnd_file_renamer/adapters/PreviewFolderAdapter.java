package com.example.alcodes_rnd_file_renamer.adapters;

import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.alcodes_rnd_file_renamer.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PreviewFolderAdapter extends RecyclerView.Adapter<PreviewFolderAdapter.ViewHolder> {

    private List<DataHolder> mData = new ArrayList<>();

    private Callbacks mCallbacks;

    @NonNull
    @Override
    public  ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_preview_file, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bindTo(mData.get(position), mCallbacks);
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public void setData(List<DataHolder> data) {
        if (data == null) {
            mData = new ArrayList<>();
        }else {
            mData = data;
        }
    }

    public void setCallbacks(Callbacks callbacks) {
        mCallbacks = callbacks;
    }

    public static class DataHolder {
        public Uri uri;
        public String OriginalName;
        public String NewName;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.linearlayout_recyclerview_root)
        public LinearLayout root;

        @BindView(R.id.imageview_file)
        public AppCompatImageView imageViewFile;

        @BindView(R.id.textview_file_original_name)
        public TextView OriginalName;

        @BindView(R.id.textview_file_new_name)
        public TextView NewName;


        public ViewHolder (@NonNull View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);
        }

        public void bindTo(DataHolder data, Callbacks callbacks) {
            resetViews();

            if (data != null){
                OriginalName.setText(data.OriginalName);
                NewName.setText(data.NewName);

                imageViewFile.setImageResource(R.drawable.ic_file);
                if (callbacks != null) {
                    root.setOnClickListener(new View.OnClickListener(){
                        @Override
                        public void onClick(View v) {
                            callbacks.onListItemClicked(data);
                        }
                    });
                }
            }
        }
        public void resetViews() {
            imageViewFile.setImageResource(R.drawable.ic_file);
            OriginalName.setText(null);
            OriginalName.setText(null);
            root.setOnClickListener(null);
        }
    }

    public interface Callbacks {
        void onListItemClicked(DataHolder data);
    }
}
