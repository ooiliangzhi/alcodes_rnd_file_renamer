package com.example.alcodes_rnd_file_renamer.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.alcodes_rnd_file_renamer.R;
import com.google.android.material.button.MaterialButton;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DisplayTaskAdapter extends RecyclerView.Adapter<DisplayTaskAdapter.ViewHolder> {

    private List<DataHolder> mData = new ArrayList<>();

    private Callbacks mCallbacks;

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_task, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bindTo(mData.get(position), mCallbacks);
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public void setData(List<DataHolder> data) {
        if (data == null) {
            mData = new ArrayList<>();
        } else {
            mData = data;
        }
    }

    public void setCallbacks(Callbacks callbacks) {
        mCallbacks = callbacks;
    }

    public static class DataHolder {

        public Long taskId;

        public String taskName;

        public Date taskCreateDate;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.linearlayout_recyclerview_task)
        public LinearLayout root;

        @BindView(R.id.textview_task_name)
        public TextView taskName;

        @BindView(R.id.textview_create_date)
        public TextView taskCreateDate;

        @BindView(R.id.button_delete)
        public MaterialButton deleteButton;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);
        }

        public void bindTo(DataHolder data, Callbacks callbacks) {
            resetViews();

            if (data != null) {
                SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
                taskName.setText(data.taskName);
                taskCreateDate.setText(dateFormat.format(data.taskCreateDate));

                if (callbacks != null) {
                    root.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            callbacks.onListItemClicked(data);
                        }
                    });
                    deleteButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            callbacks.onDeleteButtonClicked(data);
                        }
                    });
                }
            }
        }
        public void resetViews() {
            taskName.setText(null);
            taskCreateDate.setText(null);
            root.setOnClickListener(null);
        }
    }

    public interface Callbacks {
        void onListItemClicked(DataHolder data);

        void onDeleteButtonClicked(DataHolder data);
    }

}
