package com.example.alcodes_rnd_file_renamer.adapters;

import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.alcodes_rnd_file_renamer.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SelectFolderAdapter extends RecyclerView.Adapter<SelectFolderAdapter.ViewHolder> {

    private List<DataHolder> mData = new ArrayList<>();

    private Callbacks mCallbacks;

    @NonNull
    @Override
    public  ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_file, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bindTo(mData.get(position), mCallbacks);
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public void setData(List<DataHolder> data) {
        if (data == null) {
            mData = new ArrayList<>();
        }else {
            mData = data;
        }
    }

    public void setCallbacks(Callbacks callbacks) {
        mCallbacks = callbacks;
    }

    public static class DataHolder {
        public Uri uri;
        public String fileName;
        public String date;
        public long fileSize;
        public String fileType;
        public boolean check = false;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.linearlayout_recyclerview_root)
        public LinearLayout root;

        @BindView(R.id.imageview_file)
        public AppCompatImageView imageViewFile;

        @BindView(R.id.textview_file_name)
        public TextView fileName;

        @BindView(R.id.textview_file_date)
        public TextView fileDate;

        @BindView(R.id.textview_file_size)
        public TextView fileSize;

        public ViewHolder (@NonNull View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);
        }

        public void bindTo(DataHolder data, Callbacks callbacks) {
            resetViews();

            if (data != null){
                fileName.setText(data.fileName);
                fileDate.setText(data.date);
                fileSize.setText(Convertor(data.fileSize));

                if(data.check==true){
                    imageViewFile.setImageResource(R.drawable.ic_selected);
                }else{
                    imageViewFile.setImageResource(R.drawable.ic_file);
                }

                if (callbacks != null) {
                    root.setOnClickListener(new View.OnClickListener(){
                        @Override
                        public void onClick(View v) {
                            callbacks.onListItemClicked(data, imageViewFile);
                        }
                    });
                }
            }
        }
        public void resetViews() {
            imageViewFile.setImageResource(R.drawable.ic_file);
            fileName.setText(null);
            fileDate.setText(null);
            fileSize.setText(null);
            root.setOnClickListener(null);
        }
    }

    public static String Convertor(Long fileSize) {
        long n = 1024;
        String s = "";
        double kb = fileSize / n;
        double mb = kb / n;
        double gb = mb / n;
        double tb = gb / n;
        if(fileSize < n) {
            s = fileSize + " Bytes";
        } else if(fileSize >= n && fileSize < (n * n)) {
            s =  String.format("%.2f", kb) + " KB";
        } else if(fileSize >= (n * n) && fileSize < (n * n * n)) {
            s = String.format("%.2f", mb) + " MB";
        } else if(fileSize >= (n * n * n) && fileSize < (n * n * n * n)) {
            s = String.format("%.2f", gb) + " GB";
        } else if(fileSize >= (n * n * n * n)) {
            s = String.format("%.2f", tb) + " TB";
        }
        return s;
    }

    public interface Callbacks {
        void onListItemClicked(DataHolder data, AppCompatImageView imageView);
    }

}
