package com.example.alcodes_rnd_file_renamer.adapters;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.annotation.StringRes;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.example.alcodes_rnd_file_renamer.R;
import com.example.alcodes_rnd_file_renamer.fragments.CreateTaskFragment;
import com.example.alcodes_rnd_file_renamer.fragments.DisplayTaskFragment;

public class ManageTaskTabsPagerAdapter extends FragmentPagerAdapter {

    @StringRes
    private static final int[] TAB_TITLES = new int[] {R.string.create_task_name, R.string.manage_task_name};

    private final Context mContext;

    public ManageTaskTabsPagerAdapter(Context context, FragmentManager fragmentManager) {
        super(fragmentManager);
        mContext = context;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0:
                return CreateTaskFragment.newInstance();
            case 1:
                return DisplayTaskFragment.newInstance();
            default:
                return null;
        }
    }

    @NonNull
    @Override
    public  CharSequence getPageTitle(int position) {
        return  mContext.getResources().getString(TAB_TITLES[position]);
    }

    @Override
    public int getCount() {
        return 2;
    }

}
