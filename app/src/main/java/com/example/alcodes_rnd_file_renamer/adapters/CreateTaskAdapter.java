package com.example.alcodes_rnd_file_renamer.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.alcodes_rnd_file_renamer.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CreateTaskAdapter extends RecyclerView.Adapter<CreateTaskAdapter.ViewHolder> {

    private List<DataHolder> mData = new ArrayList<>();

    private Callbacks mCallbacks;

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_task_criterion, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bindTo(mData.get(position), mCallbacks);
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public void setData(List<DataHolder> data) {
        if (data == null) {
            mData = new ArrayList<>();
        } else {
            mData = data;
        }
    }

    public void setCallbacks(Callbacks callbacks) {
        mCallbacks = callbacks;
    }

    public static class DataHolder {

        public int number;
        public String title;
        public String detail;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.linearlayout_recyclerview_criterion)
        public LinearLayout root;

        @BindView(R.id.textview_number)
        public TextView number;

        @BindView(R.id.textview_criterion_type)
        public TextView title;

        @BindView(R.id.textview_criterion_detail)
        public TextView detail;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);
        }

        public void bindTo(DataHolder data, Callbacks callbacks) {
            resetViews();

            if (data != null) {
                number.setText(data.number+".");
                title.setText(data.title);
                detail.setText(data.detail);

                if (callbacks != null) {
                    root.setOnClickListener(new View.OnClickListener() {

                        @Override
                        public void onClick(View v) {
                            callbacks.onListItemClicked(data);
                        }
                    });
                }
            }
        }
        public void resetViews() {
            number.setText(null);
            title.setText(null);
            detail.setText(null);
            root.setOnClickListener(null);
        }
    }

    public interface Callbacks {
        void onListItemClicked(DataHolder data);
    }

}
