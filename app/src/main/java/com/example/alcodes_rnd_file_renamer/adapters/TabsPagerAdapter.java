package com.example.alcodes_rnd_file_renamer.adapters;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.annotation.StringRes;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.example.alcodes_rnd_file_renamer.R;
import com.example.alcodes_rnd_file_renamer.fragments.PreviewFolderFragment;
import com.example.alcodes_rnd_file_renamer.fragments.SelectFolderFragment;
import com.example.alcodes_rnd_file_renamer.fragments.SelectTaskFragment;

public class TabsPagerAdapter extends FragmentPagerAdapter {

    @StringRes
    private static final int[] TAB_TITLES = new int[] {R.string.tab_1_name, R.string.tab_2_name, R.string.tab_3_name};

    private final Context mContext;

    public TabsPagerAdapter(Context context, FragmentManager fragmentManager){
        super(fragmentManager);
        mContext = context;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0:
                return SelectFolderFragment.newInstance();
            case 1:
                return SelectTaskFragment.newInstance();
            case 2:
                return PreviewFolderFragment.newInstance();
            default:
                return null;
        }
    }

    @NonNull
    @Override
    public  CharSequence getPageTitle(int position) {
        return  mContext.getResources().getString(TAB_TITLES[position]);
    }

    @Override
    public int getCount() {
        return 3;
    }

}
