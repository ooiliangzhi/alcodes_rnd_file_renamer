package com.example.alcodes_rnd_file_renamer.viewmodels;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.example.alcodes_rnd_file_renamer.adapters.CreateTaskAdapter;
import com.example.alcodes_rnd_file_renamer.database.entities.MyTask;
import com.example.alcodes_rnd_file_renamer.repositories.CreateTaskRepository;

import java.util.List;

public class CreateTaskViewModel extends AndroidViewModel {

    private CreateTaskRepository mCreateTaskRepository;

    public CreateTaskViewModel(@NonNull Application application) {
        super(application);

        mCreateTaskRepository = CreateTaskRepository.getInstance();
    }

    public LiveData<List<CreateTaskAdapter.DataHolder>> getCreateTaskAdapterListLiveData() {

        return mCreateTaskRepository.getCreateTaskAdapterListLiveData();
    }

    public void loadCreateTaskAdapterList(List<CreateTaskAdapter.DataHolder> taskDetailList) {

        mCreateTaskRepository.loadCreateTaskAdapterList(getApplication(), taskDetailList);
    }

    public void addTask(MyTask inputTask){
        mCreateTaskRepository.addTask(getApplication(), inputTask);
    }
}
