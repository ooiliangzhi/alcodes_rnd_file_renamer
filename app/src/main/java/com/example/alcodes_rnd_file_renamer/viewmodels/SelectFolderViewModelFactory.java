package com.example.alcodes_rnd_file_renamer.viewmodels;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

public class SelectFolderViewModelFactory implements ViewModelProvider.Factory {

    private Application mApplication;

    public SelectFolderViewModelFactory(Application application) {
        mApplication = application;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        return (T) new SelectFolderViewModel(mApplication);
    }

}
