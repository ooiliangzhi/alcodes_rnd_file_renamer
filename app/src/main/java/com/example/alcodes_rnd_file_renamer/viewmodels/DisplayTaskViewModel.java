package com.example.alcodes_rnd_file_renamer.viewmodels;

import android.app.Application;
import android.content.Context;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.example.alcodes_rnd_file_renamer.adapters.DisplayTaskAdapter;
import com.example.alcodes_rnd_file_renamer.adapters.EditTaskAdapter;
import com.example.alcodes_rnd_file_renamer.database.entities.InsertCharacter;
import com.example.alcodes_rnd_file_renamer.database.entities.InsertFileSize;
import com.example.alcodes_rnd_file_renamer.database.entities.InsertNumber;
import com.example.alcodes_rnd_file_renamer.database.entities.MyTask;
import com.example.alcodes_rnd_file_renamer.database.entities.NewName;
import com.example.alcodes_rnd_file_renamer.database.entities.ReplaceCharacters;
import com.example.alcodes_rnd_file_renamer.repositories.DisplayTaskRepository;

import java.util.List;

public class DisplayTaskViewModel  extends AndroidViewModel {

    private DisplayTaskRepository mDisplayTaskRepository;

    public DisplayTaskViewModel(Application application) {
        super(application);
        mDisplayTaskRepository = DisplayTaskRepository.getInstance();
    }

    public LiveData<List<DisplayTaskAdapter.DataHolder>> getDisplayTaskAdapterListLiveData() {

        return mDisplayTaskRepository.getDisplayTaskAdapterListLiveData();
    }

    public void loadDisplayTaskAdapterList() {

        mDisplayTaskRepository.loadDisplayTaskAdapterList(getApplication());
    }

    public void deleteTask(Long id){
        mDisplayTaskRepository.deleteTask(getApplication(), id);
    }

    public void loadSingleTask(Long id) {
        mDisplayTaskRepository.loadSingleTask(getApplication(), id);
    }

    public LiveData<List<EditTaskAdapter.DataHolder>> getLoadSingleTaskCriterion() {
        return mDisplayTaskRepository.getLoadSingleTaskCriterion();
    }

    public LiveData<MyTask> getLoadSingleTask() {
        return mDisplayTaskRepository.getLoadSingleTask();
    }

    public void addReplaceCharacters(ReplaceCharacters replaceCharacters, Long taskId) {
        mDisplayTaskRepository.addReplaceCharacters(getApplication(), replaceCharacters, taskId);
    }

    public void addNewName(NewName newName, Long taskId) {
        mDisplayTaskRepository.addNewName(getApplication(), newName, taskId);
    }

    public void addInsertCharacter(InsertCharacter insertCharacter, Long taskId) {
        mDisplayTaskRepository.addInsertCharacter(getApplication(), insertCharacter, taskId);
    }

    public void addInsertNumber(InsertNumber insertNumber, Long taskId) {
        mDisplayTaskRepository.addInsertNumber(getApplication(), insertNumber, taskId);
    }

    public void addInsertFileSize(InsertFileSize insertFileSize, Long taskId) {
        mDisplayTaskRepository.addInsertFileSize(getApplication(), insertFileSize, taskId);
    }

    public void deleteCriterion(EditTaskAdapter.DataHolder data, Long taskId) {
        mDisplayTaskRepository.deleteCriterion(getApplication(), data, taskId);
    }

    public void updateTaskName(String taskName,Long taskId) {
        mDisplayTaskRepository.updateTaskName(getApplication(), taskName, taskId);
    }

}
