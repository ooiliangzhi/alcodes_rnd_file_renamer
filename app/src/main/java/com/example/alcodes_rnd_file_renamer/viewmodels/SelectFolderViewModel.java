package com.example.alcodes_rnd_file_renamer.viewmodels;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.example.alcodes_rnd_file_renamer.adapters.DisplaySelectionTaskAdapter;
import com.example.alcodes_rnd_file_renamer.adapters.PreviewFolderAdapter;
import com.example.alcodes_rnd_file_renamer.adapters.SelectFolderAdapter;
import com.example.alcodes_rnd_file_renamer.repositories.SelectFolderRepository;

import java.io.FileNotFoundException;
import java.util.List;

public class SelectFolderViewModel extends AndroidViewModel {

    private SelectFolderRepository mSelectFolderRepository;

    public SelectFolderViewModel(@NonNull Application application) {
        super(application);

        mSelectFolderRepository = SelectFolderRepository.getInstance();
    }

    public LiveData<List<SelectFolderAdapter.DataHolder>> getSelectFolderAdapterListLiveData() {

        return mSelectFolderRepository.getSelectFolderAdapterListLiveData();
    }

    public void loadSelectFolderAdapterList(List<SelectFolderAdapter.DataHolder> fileDetailModelList) {

        mSelectFolderRepository.loadSelectFolderAdapterList(getApplication(), fileDetailModelList);
    }

    public void updateSingleSelectFolder(SelectFolderAdapter.DataHolder data) {

        mSelectFolderRepository.updateSingleSelectFolder(getApplication(), data);
    }

    public void updateSelectFolderAdapterList(boolean checkSelectedAll) {

        mSelectFolderRepository.updateSelectFolderAdapterList(getApplication(), checkSelectedAll);
    }

    public void loadDisplaySelectionTaskAdapterList(){
        mSelectFolderRepository.loadDisplaySelectionTaskAdapterList(getApplication());
    }

    public LiveData<List<DisplaySelectionTaskAdapter.DataHolder>> getLoadDisplaySelectionTaskAdapterList(){
        return mSelectFolderRepository.getLoadDisplaySelectionTaskAdapterList();
    }

    public void updateDisplaySelectionTaskAdapter(DisplaySelectionTaskAdapter.DataHolder data){
        mSelectFolderRepository.updateDisplaySelectionTaskAdapter(getApplication(), data);
    }

    public void loadPreviewRenameFile(){
        mSelectFolderRepository.loadPreviewRenameFile(getApplication());
    }

    public LiveData<List<PreviewFolderAdapter.DataHolder>> getLoadPreviewRenameFile(){
        return mSelectFolderRepository.getLoadPreviewRenameFile();
    }

    public void saveRenameFile() {
        mSelectFolderRepository.saveRenameFile(getApplication());
    }
}
