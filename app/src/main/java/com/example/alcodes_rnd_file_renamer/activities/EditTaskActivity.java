package com.example.alcodes_rnd_file_renamer.activities;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;

import com.example.alcodes_rnd_file_renamer.R;
import com.example.alcodes_rnd_file_renamer.fragments.EditTaskFragment;

import butterknife.ButterKnife;

public class EditTaskActivity extends AppCompatActivity {

    public static final String EXTRA_LONG_MY_TASK_ID = "EXTRA_LONG_MY_TASK_ID";

    public static final int RESULT_CONTENT_MODIFIED = 200;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_edit_task);

        ButterKnife.bind(this);

        FragmentManager fragmentManager = getSupportFragmentManager();

        if (fragmentManager.findFragmentByTag(EditTaskFragment.TAG) == null) {
            Intent extra = getIntent();
            long taskId = 0;

            if(extra != null) {
                taskId = extra.getLongExtra(EXTRA_LONG_MY_TASK_ID, 0);
            }

            fragmentManager.beginTransaction()
                    .replace(R.id.framelayout_fragment_holder,EditTaskFragment.newInstance(taskId),EditTaskFragment.TAG)
                    .commit();
        }
    }
}
