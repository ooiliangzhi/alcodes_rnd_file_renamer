package com.example.alcodes_rnd_file_renamer.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;

import android.os.Bundle;

import com.example.alcodes_rnd_file_renamer.R;
import com.example.alcodes_rnd_file_renamer.fragments.MainFragment;

import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);

        FragmentManager fragmentManager = getSupportFragmentManager();

        if (fragmentManager.findFragmentByTag(MainFragment.TAG) == null) {
            fragmentManager.beginTransaction()
                    .replace(R.id.framelayout_fragment_holder, MainFragment.newInstance(), MainFragment.TAG)
                    .commit();
        }
    }

}
